jQuery(document).ready(function($) {
	$('.qty').on('change',function(){
		var price = $(this).data('price');
        var key = $(this).data('key');
		var qty =  $(this).val();
		if (qty > 10 && !$(this).hasClass('accessory')) {
			alert('More then 10 panels are not allowed. For large orders please contact the team directly at sales@pixaluxmanufacturing.com.au or on 1300 113 532.');
			$('.qty-'+key).val(10);
			return false;
		} else {
			$key = $(this).data('key');
			$.ajax({
				url: 'cartaddqty.php',
				type: 'POST',
				data: { index: $key, quantity : qty},
			})
				.done(function (data) {
					price = (qty * price);
					//$('.price-' + key).val(price.toFixed(2));
					$('.price-' + key).html(price.toFixed(2));
					$('.qty-' + key).val(qty);
					calculateTotal();
				})
			
		}
        
    });
    calculateTotal();
    function calculateTotal(){

		$totalQty = 0;
		$totalPrice = $totalPrice1 = $shipping = 0.0;
		$totalQtyForDisplay = 0;
		$shippingCharge = parseFloat($('.shipping_charge').html());
		
	    $.each($('.price_loop :input'),function(index, el) {
			if(el.getAttribute('data-tag') == 'qty'){
				$totalQty = parseInt($totalQty) + parseInt(el.value);
				
			} 
			if (el.getAttribute('data-tag') == 'price') {
				if($totalQty > 0){
					$totalPrice1 =  parseFloat(el.value);
					$totalPrice1 = $totalPrice1 * $totalQty;
					$totalPrice = $totalPrice + $totalPrice1;
					$totalQtyForDisplay = $totalQtyForDisplay + $totalQty;
					$totalQty = 0; 
				}
	    	}
		});
		$totalPrice = $totalPrice.toFixed(2)
		var $freight_rate = parseFloat($('#freight_rate').val());
		var $min_frieght =  parseFloat($('#min_frieght').val());
		var $max_frieght =  parseFloat($('#max_frieght').val());

		var $shipping = $totalPrice * $freight_rate/100;
		if($shipping > 0){
			if ($shipping <= $min_frieght ){
				$shipping = $min_frieght;
			} else if ($shipping >= $max_frieght){
				$shipping = $max_frieght;
			}
		}
		if(!($shipping > 0) || $shipping == 'NaN'){
			$shipping = 0.00;
		}
		
		if ($totalPrice == 0){
			$shipping = 0.00;
		}
		$shipping = $shipping.toFixed(2);
		console.log($shipping);
		$totalPrice = ($totalPrice*1).toFixed(2);
		$('.shipping_charge').html($shipping);
		$('.shipping_charge').val($shipping);
		$('.total_qty_disp').html($totalQtyForDisplay);
		$('.total_qty_disp').val($totalQtyForDisplay);
		$('#cartCount').html($totalQtyForDisplay)
	    $('.total_price_disp').html($totalPrice);
		$('.total_price_disp').val($totalPrice);
		var totalPriceWithShipping = parseFloat($totalPrice) + parseFloat($shipping);
		$('.total_price_text').val((parseFloat($totalPrice) + parseFloat($shipping)).toFixed(2));
		$('.total_price_text').html((parseFloat($totalPrice) + parseFloat($shipping)).toFixed(2));
		var tax = $('.tax').val();
		var taxPrice = $('.total_price_text').val() * tax ;
		$('.total_tax').val(parseFloat(taxPrice).toFixed(2));
		$('.total_tax').html(parseFloat(taxPrice).toFixed(2));
		var totalPriceWithTax = totalPriceWithShipping + taxPrice
		$('.total_price_with_tax').val(totalPriceWithTax.toFixed(2))
		$('.total_price_with_tax').html(totalPriceWithTax.toFixed(2))


	}
	
	
			$('.remove-from-cart').on('click',function(){
				$key = $(this).data('key');
				$.ajax({
					url: 'session.php',
					type: 'POST',
					data: {key: $key},
				})
				.done(function(data) {
					$('.data-'+$key).remove();
					location.reload(true);
				})
				.fail(function() {
					console.log("error");
				});
			});

	            $("#SvgjsSvg1001").attr({
	            	viewBox: "0 0 500 500"
	            });
			setTimeout(function() {
	        }, 10);
		
});