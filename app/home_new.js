var draw = SVG('pixalux').size(DrawingSize, DrawingSize);

profileDrawing();
Dropdown('led', 'Led');
Dropdown('cexit', 'Arrow');
Dropdown('side', 'Lit');
Draw('top', false, 5);
Draw('bot', false, 5);

global_olength = $("#length").val();
global_odepth = $("#depth").val();
if ($('input[name="frame_color"]').val() == ''){
    global_ncolor = $("#mycp2").val();
}else{
    global_ncolor = $('input[name="frame_color"]').val();
}
var global_cexit;
var global_chexit;

var profileNeed = '';
var frameColorForSummary = '';
var ledColor = '';					 
setTimeout(function(){
    draw_pixalux(global_olength, global_odepth, global_ncolor, '#DAE1E5', 16, global_cexit, global_chexit);
},500);

$(document).ready(function () {
	
    $("#length").on("change click", function () {
        pixalux('length');
    });

    $("#depth").on("change click", function () {
        pixalux('depth');
    });

    $("#led").on("change paste keyup", function () {
        $('#headingOne').css("background-color", "#e3fce8");
        $('input[name="led_name"]').val($(this).find('option:selected').text());
        pixalux('led');
    });

    $("#side").on("change paste keyup", function () {
        $('#headingTwo').css("background-color", "#e3fce8");
        $('input[name="lit_name"]').val($(this).find('option:selected').text());
        pixalux('side');
    });

    $('#button1,#button2,#button3,#button4,#button5,#button6,#button7,#button8').on("click", function () {
        $('input[name="profile_name"]').val($(this).val());
        pixalux('button',$(this).val());
    });

    $("#mycp1 , .mycp1").on("change click paste keyup", function () {
        frameColorForSummary = $('.mycp1').html();
        $("#mycp1").val('#000');
        $('#headingFive').css("background-color", "#e3fce8");
        $('input[name="frame_color"]').val($("#mycp1").val());
        pixalux('color',$("#mycp1").val());
    });

    $("#mycp2, .mycp2").on("change click paste keyup", function () {
        frameColorForSummary = $('.mycp2').html();
        $("#mycp2").val('#666');
        $('#headingFive').css("background-color", "#e3fce8");
        $('input[name="frame_color"]').val($("#mycp2").val());
        pixalux('color',$("#mycp2").val());
    });

    $("#mycp3 , .mycp3").on("change click paste keyup", function () {
        frameColorForSummary = $('.mycp3').html();
        $("#mycp3").val('#fff');
        $('#headingFive').css("background-color", "#e3fce8");
        $('input[name="frame_color"]').val($("#mycp3").val());
		pixalux('color',$("#mycp3").val());
    });

    $("#cexit").on("change paste keyup", function () {
        $('#headingSix').css("background-color", "#e3fce8");
        $('input[name="cable_side"]').val($(this).find('option:selected').text());
        pixalux('cexit');
    });

    $("#chexit").on("change paste keyup", function () {
        $('#headingSix').css("background-color", "#e3fce8");
        $('input[name="cable_exit"]').val($(this).find('option:selected').text());
        pixalux('chexit'); 
    });

	

    var profile;
    var color;
    if($('input[name="key"]').val() != ''){
        profile = $('input[name="profile_name"]').val();
        $('#led').val($('.led_id').data('id'));
        
        $('#side').val($('.side_id').data('id'));
        $('#cexit').val($('.cexit_id').data('id'));
        $('#chexit').val($('.chexit_id').data('id')); 

        //$('.drawprofile-'+profile).addClass('highlight');

        color = $('input[name="frame_color"]').val();

        if(color == '#000'){
            $(".mycp1").addClass('border');
            frameColorForSummary = $('.mycp1').html();
        }
        if(color == '#666'){
            frameColorForSummary = $('.mycp2').html();
            $(".mycp2").addClass('border');
        }
        if(color == '#fff'){
            frameColorForSummary = $('.mycp3').html();
            $(".mycp3").addClass('border');
        }
        $('#led').trigger("onchange");
        console.log('.drawprofile-' + profile);
        profileNeed = $('.drawprofile-' + profile);
        console.log(profileNeed);
        setTimeout(function() {
            
            pixalux('length');
            pixalux('depth');
            pixalux('led');
            pixalux('side');
            pixalux('button',$('input[name="profile_name"]').val());
            pixalux('color',$('input[name="frame_color"]').val());
            pixalux('cexit');
            pixalux('chexit');
            pixalux('color', $('input[name="frame_color"]').val());
            
        }, 500);

        $('.edited').css("background-color", "#e3fce8");
        
    }

    function pixalux(key = '',value=''){
        global_olength = $("#length").val();
        global_odepth = $("#depth").val();
        global_LedID = $("#led").find('option:selected').val();
        global_BotsideID = $("#side").find('option:selected').val();
        global_cexit = $('#cexit').find('option:selected').text();
        global_CableID = $('#cexit').val();
        global_chexit = $("#chexit").val();
        var global_BotprofileID;

        global_ledcolor = $("#colour").val();
        global_bheight = $("#height").val();
        global_nedge = $("#edge").val();
        global_ltype = $("#LineType").val();
        global_pitch = $("#pitch").val();

        if(key == 'button'){
			
            global_BotprofileID = value;
            // console.log(global_BotprofileID)
            if(global_BotprofileID != profile){
                // console.log('herher',profile);
                $('.drawprofile-'+profile).removeClass('highlight');
                profile = global_BotprofileID;
                $('.drawprofile-'+global_BotprofileID).addClass('highlight');
            }
            if (global_BotprofileID == 6) {
                console.log(global_odepth);
                if (global_odepth > 400) {
                    $('#headingFour').removeAttr('background-color');
                    $('#depth').css('color', 'red')
                    $('#headingFour').find('a').trigger('click');

                    var lsizetext = $('#lsizetext').html();
                    if ($('.panelStandError').length == 0) {
                        $('#lsizetext').html(lsizetext + '<li class="panelStandError">For Profile "Panel Stand" , Max Depth is 400mm </li>');
                        $('#lsizetext').html(lsizetext + '<li class="panelStandError">For Profile "Panel Stand" , Max Depth is 400mm </li>');
                        $("#lsizetext").css("color", "red");
                        $('#headingFour').addClass('error');
                        $('#headingThree').addClass('error');
                    }
                   // return false;
                }
            }

            global_TopprofileID = BotTopNew(global_BotprofileID);
            Draw('bot', false, false, global_BotprofileID);
            findHeight(global_BotprofileID);
            findMaxLength(global_odepth,global_BotprofileID);
            update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        }
        
        if(key == 'color'){
            global_ncolor = value;
            if(global_ncolor != color){
                $(".mycp1").removeClass('border');
                $(".mycp2").removeClass('border');
                $(".mycp3").removeClass('border');
                color = value;
                if(global_ncolor == '#000'){
                    $(".mycp1").addClass('border');
                }
                if(global_ncolor == '#666'){
                    $(".mycp2").addClass('border');
                }
                if(global_ncolor == '#fff'){
                    $(".mycp3").addClass('border');
                }
            }
            update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        }else{
            if(color == 'undefined' && color == ""){
                global_ncolor = color;
            }else{
                global_ncolor = $("#mycp2").val();
            }
        }

        if(key == 'led'){
            global_ncolor = '#666';
            findPitch(global_LedID);
            findColour(global_LedID, "colour");
            Draw('top', global_LedID);
            Draw('bot', global_LedID);
        }

        if(key == 'cexit'){
            Draw('bot', false, false, false, global_CableID);
            global_TopCableID = BotTopNew(global_CableID);
            findMaxLength(global_odepth,global_BotprofileID);
            update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        }

        if(key == 'side'){
            global_TopsideID = BotTopNew(global_BotsideID);
            Draw('top', false, global_TopsideID);
            Draw('bot', false, global_BotsideID);
            update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        }

        if(key == 'chexit'){
            Draw('bot', false, false, false, global_CableID);
            update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
        }
        var lengthDepth = true
        $('#headingFour').css("background-color", "rgba(0,0,0,.03)");
        if(key == 'length'){

            findMaxLength(global_odepth,global_BotprofileID);
            if($('#collapseFour ul').hasClass('lengthDepth')){
                lengthDepth = false;
            }
        }
        
        if(key == 'depth'){
           // CheckDepth(global_odepth);
            if ($('#collapseFour ul').hasClass('lengthDepth')) {
                lengthDepth = false;
            }
            findMaxLength(global_odepth,global_BotprofileID);
        }
        if (lengthDepth){
            $('#headingFour').css("background-color", "#e3fce8");
        }
    }
});

function getPowerForLed(depth, led) {
    const url = home_url + "/drawing/read_power.php?depth=" + depth + "&led=" + led;
    $.getJSON(url, function (data) {
        return data.records.power;
    });

}

function drawprofile(profileID) {
    profileNeed = $('.drawprofile-' + profileID).text();
    $('#headingThree').css("background-color", "#e3fce8");
    $('input[name="profile_name"]').val(profileID);
    global_odepth = $("#depth").val();
    global_olength = $("#length").val();
    global_ncolor = $('#mycp2').val();
    global_cexit = $('#cexit').find('option:selected').text();
    global_ledcolor = $("#colour").val();
    global_bheight = $("#height").val();
    global_nedge = $("#edge").val();
    global_BotprofileID = profileID;
    console.log(profileID);
    //BotTopNew(global_BotprofileID);
    global_ltype = $("#LineType").val();
    Draw('bot', false, false, global_BotprofileID);
    findHeight(global_BotprofileID);
    findMaxLength(global_odepth,global_BotprofileID);
};

$(function() { 
    $('#editPincode').on("click",function(){
        if (confirm('If you change pincode, then all product will be use same pincode')) {
            $("#pincodeForCart").removeAttr("readonly");
        }else{
            $("#pincodeForCart").attr("readonly", "readonly");;
        } 
    });
    $("#checkSummary").on("click", function () {
        //console.log($('.drawprofile-' + $('input[name="profile_name"]').val()).text());
        $("#lengthDetail").html($("#length").val());
        $("#depthDetail").html($("#depth").val());
        $("#ledDetail").html($("#led option:selected").text());
        $("#litDetail").html($("#side option:selected").text());
        $("#profileChange").html($('.drawprofile-' + $('input[name="profile_name"]').val()).text())
        $("#frameColor").html(frameColorForSummary);
        $("#sideExit").html($("#cexit option:selected").text());
        console.log($("#chexit option:selected").text());
        $("#cableExit").html($("#chexit option:selected").text());
		var power;
        url = home_url + "/drawing/read_one.php?id=" + $("#led").val();
        $.getJSON(url, function (data) {
			console.log('volatage' );
            console.log(data.value.Voltage);
			power = data.value.Power
            $("#voltageLed").html(data.value.Voltage + ' V');
            $("#voltageled").val(data.value.Voltage);
        });
		console.log(power);
		var url = home_url + "/drawing/read_power.php?depth=" + $("#depth").val() + "&led=" + $("#led").val();
        
        $.getJSON(url, function (data) {
			console.log(data);
            var price = data.records.price;
			console.log('power' , power);
            //$("#powerLed").html((data.records.power * $("#length").val()/1000).toFixed(2) + ' W');
            //$("#powerled").val((data.records.power * $("#length").val()/1000).toFixed(2));
			var length = $("#length").val();
			price = price * length/1000;
			//power = power * length/1000;
			var searchlength = length;
			switch(true){
				case (searchlength < 200):
					searchlength=200;
					id=52;
					$("#length").val(200);
					$("#length").css("color","red");
					$("#mintext").css("color","red");
					$('#collapseFour ul').addClass('lengthDepth');
					break;
				case (searchlength <= 1200):
					id=52;
					break;
				case (searchlength <= 1500):
					id=50;
					break;
				case (searchlength <= 1800):
					id=49;
					break;
				case (searchlength >= 1801):
					id=49;
					searchlength = 1800;
					$("#length").val(1800);
					$("#length").css("color","red");
					$('#collapseFour ul').addClass('lengthDepth');
					tmaxlength='<li style="color:red">Maximum Length is 1800mm.</li>';
					break;
			}
			var edge = 1;
			url2 = home_url + "/drawing/read_one.php?id="+id;
			$.getJSON(url2, function (data) {
				if(data.value.Edge > 0 && $("#depth").val() > 400 ){
					edge = data.value.Edge
				} 
				
				power = power * length/1000;
				console.log('edge, power ' );
				console.log( edge,power)
				console.log('calculat' , (power * edge ).toFixed(2))
				if(power > 0){
					$("#powerLed").html((power * edge ).toFixed(2) + ' W');
					$("#powerled").val((power *edge ).toFixed(2));
				}
			});
			$("#priceLed").html('$ '+ price.toFixed(2));
			$("#priceLed").val(price.toFixed(2));
			
		});
        $("#profile").html(profileNeed);

    });

    $(".cart_button").click(function(e) { 
        e.preventDefault();
        $('.card-header').removeClass('error');
        $('#qtyError').remove();
        console.log($('input[name="led_name"]').val());
        var led_name = $('input[name="led_name"]').val()
        var lit_name = $('input[name="lit_name"]').val()
        var profile_name = $('input[name="profile_name"]').val()
        var cable_side = $('input[name="cable_side"]').val()
        var cable_exit = $('input[name="cable_exit"]').val()
        var length = $('#length').val()
        var depth = $('#depth').val()
        var frameColor = $('#mycp2').val()
        var pincode = $('#pincodeForCart').val()
        if(length == ''){
            $('#headingFour').addClass('error');return false
        }
        if(depth == ''){
            $('#headingFour').addClass('error');return false
        }
        if(led_name == ''){
            $('#headingOne').addClass('error');return false
        }
        if(lit_name == ''){
            $('#headingTwo').addClass('error');return false
        }
        if(profile_name == ''){
            $('#headingThree').addClass('error');return false
        }
        if (profile_name == 6) {
            console.log(depth);
            if (depth > 400) {
                $('#headingFour').removeAttr('background-color');
                $('#depth').css('color', 'red')
                $('#headingFour').find('a').trigger('click');

                var lsizetext = $('#lsizetext').html();
                if ($('.panelStandError').length == 0) {
                    $('#lsizetext').html(lsizetext + '<li class="panelStandError">For Profile "Panel Stand" , Max Depth is 400mm </li>');
                    $('#lsizetext').html(lsizetext + '<li class="panelStandError">For Profile "Panel Stand" , Max Depth is 400mm </li>');
                    $("#lsizetext").css("color", "red");
                    $('#headingFour').addClass('error');
                    $('#headingThree').addClass('error'); 
                }
                return false;
            }
        }
        if(frameColor == ''){
            $('#headingFive').addClass('error');return false
        }
        if(cable_side == ''){
            $('#headingSix').addClass('error');return false
        }
        if(cable_exit == ''){
            $('#headingSix').addClass('error');return false
        }
        if ($('#qty').val() == 0) {
            $("#headingSeven").addClass('error');
            $("#qty").addClass('error');
            $('.qty-error').addClass('error');
            $('.qty-error').append('<span id="qtyError">Please Enter atleast 1 Quantity</span>')
            return false
        }
		var qty = $('#qty').val();
		var er = /^-?[0-9]+$/;
        if(!er.test(qty)){
            $("#headingSeven").addClass('error');
            $("#qty").addClass('error');
            $('.qty-error').addClass('error');
            $('.qty-error').append('<span id="qtyError">Please enter numeric Quantity</span>')
            return false
        }
        if($('#qty').val() > 10){
            $("#headingSeven").addClass('error');
            $("#qty").addClass('error');
            $('.qty-error').addClass('error');
            $('.qty-error').append('<span id="qtyError">Please Max 10 Quantity</span>')
            return false
        }
         
        if (pincode == ''){
            $('.forPincode').addClass('error');return false
        }else{
            url = home_url + "/drawing/read_postcode.php?postcode=" + pincode;
            $.getJSON(url, function (data) {
                if(!data.status){
                    alert('Please enter Postcode for Australia.')
                    return false
                }else{
                    var loaderImage = 'img/loading.gif';
                    var elem = document.createElement('div'); // Create Div for Loader
                    elem.style.cssText = 'position:fixed;width:100%;height:100%;opacity:0.3;z-index:100;background:url("' + loaderImage + '") no-repeat center center rgba(0,0,0,0.25)';
                    elem.setAttribute("id", "facebookLoaderByQbix");
                    var container = document.body;
                    container.insertBefore(elem, container.firstChild);
                    var ua = navigator.userAgent;

                    var node = document.getElementById('svgCanvas');
                    domtoimage.toPng(node)
                        .then(function (dataUrl) {

                            $('input[name="first_part"]').val(dataUrl);
                        })
                        .catch(function (error) {
                            console.error('oops, something went wrong!', error);
                        });


                    var node1 = document.getElementById('pixalux');
                    domtoimage.toPng(node1)
                        .then(function (dataUrl) {

                            $('input[name="second_part"]').val(dataUrl);
                        })
                        .catch(function (error) {
                            console.error('oops, something went wrong!', error);
                        });


                    setTimeout(function () {
                        $('#submit_form').submit();
                    }, 5000);
                }
            });
        }
        

        // if(profile_name == 6){
        //     console.log(depth);
        //     if(depth > 400){
        //         $('#headingFour').removeAttr('background-color');
        //         $('#depth').css('color','red')
        //         $('#headingFour').find('a').trigger('click');
            
        //         var lsizetext = $('#lsizetext').html();
        //         if($('.panelStandError').length == 0){
        //             $('#lsizetext').html(lsizetext + '<li class="panelStandError">For Profile "Panel Stand" , Max Depth is 400mm </li>');
        //             $("#lsizetext").css("color", "red");
        //             $('#headingFour').addClass('error');
        //             $('#headingThree').addClass('error');return false;
        //         }
        //         return false;
        //     } 
        // }

        
        
        
    });

    
}); 

