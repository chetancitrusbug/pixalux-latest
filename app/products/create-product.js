$(document).ready(function() {
  // will run if create product form was submitted
  $(document).on("submit", "#create-product-form", function() {
    // get form data

    var form_raw_data = {
      lenght: global_olength,
      depth: global_odepth,
      edge_color: global_ncolor,
      Line_type: global_ltype,
      led_color: global_ledcolor,
      number_edge: global_nedge,
      exit_topbottom: global_cexit,
      exit_leftrtight: global_chexit,
      border_height: global_bheight,
      led_pitch: global_pitch,
      led_id: global_LedID,
      botside_id: global_BotsideID,
      botprofile_id: global_BotprofileID,
      topside_id: global_TopsideID,
      topprofile_id: global_TopprofileID,
      cable_id: global_CableID
    };

    var form_data = JSON.stringify(form_raw_data);
    //console.log(form_raw_data);
    //console.log(form_data);
    // submit form data to api
    $.ajax({
      url: home_url + "/product/create.php",
      type: "POST",
      contentType: "application/json",
      data: form_data,
      success: function(result) {
        // product was created, go back to products list
        // console.log('Created');
        $("#success-alert")
          .fadeTo(2000, 500)
          .slideUp(500, function() {
            $("#success-alert").slideUp(500);
          });
      },
      error: function(xhr, resp, text) {
        // show error to console
        //console.log(xhr, resp, text);
      }
    });

    return false;
  });
});
