function profileDrawing() {

    const url = home_url + "/drawing/read_type.php?type=profile";
    var profileName = $('input[name="profile_name"]').val();
    var className = '';
    $.getJSON(url, function (data) {
        profile_html="<div class='row'>";
        var i=1;

        $.each(data.records, function(key, val){
            var className = '';
            profile_html+="<div class='col'>";
                profile_html+="<svg xmlns='http://www.w3.org/2000/svg' viewBox='30 90 220 90'>";
                profile_html+=val.content;
                profile_html+="</svg>";
                profile_html+="<div class='text-center'>";
                if(val.id == profileName) 
                        className = 'highlight';
                    profile_html+="<button id='button"+i+"' class='btn btn-outline-secondary drawprofile-"+val.id+ ' ' + className +"' type='button' value="+val.id+" onclick='drawprofile("+val.id+")'>"+val.name+"</button>";
                    profile_html+="</div>";
            profile_html+="</div>";
            if ((i % 2) == 0) {
                profile_html+="</div><div class='row'>";
            }
            i++;
        })
        profile_html+="</div>";
        $("#profileDrawing").html(profile_html);
        //console.log(profile_html);
    });     
}



function Dropdown(elementID,dtype) {
    let dropdown = $('#'+elementID);

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Choose</option>');
    dropdown.prop('selectedIndex', 0);

    const url = home_url + "/drawing/read_type.php?type="+dtype;

    // Populate dropdown with list of provinces
    $.getJSON(url, function (data) {
        
        $.each(data.records, function(key, val){
            if(elementID == 'led'){
                if(val.id == $('.led_id').data('id')){
                    dropdown.append($('<option></option>').attr('value', val.id).text(val.name).attr("selected", "selected"));
                }else{
                    dropdown.append($('<option></option>').attr('value', val.id).text(val.name));
                }
            }
            if(elementID == 'side'){
                if(val.id == $('.side_id').data('id')){
                    dropdown.append($('<option></option>').attr('value', val.id).text(val.name).attr("selected", "selected"));
                }else{
                    dropdown.append($('<option></option>').attr('value', val.id).text(val.name));
                }
            }
            if (elementID == 'cexit') {
                if (val.id == $('.cexit_id').data('id')) {
                    dropdown.append($('<option></option>').attr('value', val.id).text(val.name).attr("selected", "selected"));
                } else {
                    dropdown.append($('<option></option>').attr('value', val.id).text(val.name));
                }
            }
            //dropdown.append($('<option></option>').attr('value', val.id).text(val.name));
        })
    });     
    //console.log('Dropdown: '+elementID);
}

function findPitch(settingID) {
    //console.log('FindPitch');
    const url = home_url + "/drawing/read_one.php?id="+settingID;
    $.getJSON(url, function (data) {
        $("#pitchtext").html('Pitch for the '+data.name+' is <strong><span id="pitchvalue"></span>mm</strong>. For optimal panel length, we recommend that the length is a multiple of '+data.value.Pitch+'mm + 15mm.');
        $("#pitchvalue").html(data.value.Pitch);
        $("#pitch").val(data.value.Pitch);
    });
}

function findColour(settingID,elementID) {
    //console.log('FindColour');
    const url = home_url + "/drawing/read_one.php?id="+settingID;
    $.getJSON(url, function (data) {
        $("#"+elementID).val(data.value.Colour);
        update_pixalux(global_olength, global_odepth, global_ncolor, data.value.Colour, global_bheight, global_ltype, global_nedge, global_cexit, global_chexit);
    });
}

function findHeight(settingID) {
    //console.log('FindHeight');
    var ltype=null;
    const url = home_url + "/drawing/read_one.php?id="+settingID;
    $.getJSON(url, function (data) {
        $("#height").val(data.value.Height);
        if (settingID=='9' || settingID=='10') ltype=data.value.Line;
        //console.log('ltype: '+ltype);
        $("#LineType").val(data.value.Line);
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, data.value.Height, data.value.Line, global_nedge, global_cexit, global_chexit);
    });
}

function findMaxDepth(searchlength,pitch) {
    $('#collapseFour ul').removeClass('lengthDepth');
    pitch = pitch || 50;
    $("#length").css("color","#555");
    $("#dsizetext").css("color","#555");
    tmaxlength='';
    
    switch(true){
        case (searchlength < 200):
            searchlength=200;
            id=52;
            $("#length").val(200);
            $("#length").css("color","red");
            $("#mintext").css("color","red");
            $('#collapseFour ul').addClass('lengthDepth');
            break;
        case (searchlength <= 1200):
            id=52;
            break;
        case (searchlength <= 1500):
            id=50;
            break;
        case (searchlength <= 1800):
            id=49;
            break;
        case (searchlength >= 1801):
            id=49;
            searchlength = 1800;
            $("#length").val(1800);
            $("#length").css("color","red");
            $('#collapseFour ul').addClass('lengthDepth');
            tmaxlength='<li style="color:red">Maximum Length is 1800mm.</li>';
            break;
    }

    ideallength1=(parseInt((searchlength-15)/pitch)*pitch)+15;
    ideallength2=((parseInt((searchlength-15)/pitch)+1)*pitch)+15;
    
    //console.log(id);
    const url = home_url + "/drawing/read_one.php?id="+id;
    $.getJSON(url, function (data) {
        $("#depth").attr({
            "max" : data.value.Depth,
            "min" : 200
        });
        $("#newdepth").val(data.value.Depth);
        if ($("#depth").val()>data.value.Depth) {
            $("#depth").val(data.value.Depth); 
            $("#depth").css("color","red");
            $("#dsizetext").css("color","red");
            $('#collapseFour ul').addClass('lengthDepth');
        }
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, data.value.Edge, global_cexit, global_chexit);
        //console.log('findConstraints: '+data.value.Depth);
        $("#dsizetext").html(tmaxlength+'<li>For the Lenght of '+searchlength+'mm, the maximum Depth is <span id="depthtext"></span>mm.</li><li>With chosen LED, please consider Ideal Lenght of '+ideallength1+'mm or '+ideallength2+'mm.</li>');
        $("#depthtext").html(data.value.Depth);
    });
    // var elem = $('#collapseFour');
    // var message = elem[0].querySelectorAll('.message');
    // for (var i = 0; i < message.length; i++) {
    //     console.log(message[i].hasOwnProperty('color'));
    // }
}

function findMaxLength(searchDepth,BotprofileID) {
    $('#collapseFour ul').removeClass('lengthDepth');
    $("#depth").css("color","#555");
    $("#lsizetext").css("color","#555");
    tmaxdepth='';
    
    switch(true){
        case (searchDepth < 199):
            searchDepth=200;
            edges=1;
            $("#depth").val(200);
            $("#depth").css("color","red");
            $("#mintext").css("color","red");
            $('#collapseFour ul').addClass('lengthDepth');
            id=49;
            break;
        case (searchDepth <= 300):
            id=49;
            edges=1;
            break;
        case (searchDepth <= 400):
            id=50;
            edges=2;
            break;
        case (searchDepth <= 800):
            id=52;
            edges=2;
            break;
        case (searchDepth >= 800):
            searchDepth=800;
            edges=2;
            $("#depth").val(800);
            $("#depth").css("color","red");
            tmaxdepth='<li style="color:red">Maximum Depth is 800mm.</li>';
            $('#collapseFour ul').addClass('lengthDepth');
            id=52;
            break;
    }
    //console.log(id);
    const url = home_url + "/drawing/read_one.php?id="+id;
    $.getJSON(url, function (data) {
        $("#length").attr({
            "max" : data.value.Length,
            "min" : 200
        });
        $("#newlength").val(data.value.Length);
        $("#edge").val(data.value.Edge);
        if($("#length").val() < 200){
            $("#length").val(200);
            $("#length").css("color", "red");
            $("#lsizetext").css("color", "red");
            $('#collapseFour ul').addClass('lengthDepth');
        }
        if ($("#length").val()>data.value.Length) {
            $("#length").val(data.value.Length); 
            $("#length").css("color","red");
            $("#lsizetext").css("color","red");
            $('#collapseFour ul').addClass('lengthDepth');
        }
        
        if (data.value.Edge==2 && edges==2 && BotprofileID!=6) {
			global_TopprofileID=BotTopNew(BotprofileID);
            Draw('top', false, false, global_TopprofileID);
			global_TopCableID=BotTopNew(global_CableID);
            Draw('top', false, false, false, global_TopCableID);
            //console.log('Add Top');
        } else {
            RemoveDraw('top');
            //console.log('Remove Top');
        }
        update_pixalux(global_olength, global_odepth, global_ncolor, global_ledcolor, global_bheight, global_ltype, data.value.Edge, global_cexit, global_chexit);
        //console.log('findConstraints: '+data.value.Length);
        $("#lsizetext").html(tmaxdepth+'<li>For the Depth of '+searchDepth+'mm, the maximum Length is <span id="lengthtext"></span>mm and minmum length is 200mm.</li>');
        $("#lengthtext").html(data.value.Length);
    });
    
    
    //console.log(elem[0].querySelectorAll('.message'));
}

function Draw(place,ledID,baseID,profileID,cableID) {
    if (ledID){
        //console.log('ledID');
        const url = home_url + "/drawing/read_one.php?id="+ledID;
        $.getJSON(url, function (data) {
            newcontent=data.content;
            $('#'+place+'ledcolor').html(newcontent);
            //console.log(newcontent);
        });
    }
    if (baseID){
        //console.log('baseID');
        const url = home_url + "/drawing/read_one.php?id="+baseID;
        $.getJSON(url, function (data) {
            newcontent=data.content;
            $('#'+place+'base').html(newcontent);
            //console.log(newcontent);
        });
    }
    if (profileID){
        //console.log('profileID');
        //onsole.log(document.getElementsByClassName('btn-outline-secondary'));
        $('.btn-outline-secondary.highlight').removeClass('highlight');
        $('.drawprofile-' + profileID).addClass('highlight');
        const url = home_url + "/drawing/read_one.php?id="+profileID;
        $.getJSON(url, function (data) {
            newcontent=data.content;
            $('#'+place+'poly').html(newcontent);
        });
    }
    if (cableID){
        //console.log('cableID');
        const url = home_url + "/drawing/read_one.php?id="+cableID;
        $.getJSON(url, function (data) {
            newcontent=data.content;
            $('#'+place+'cable').html(newcontent);
            //console.log(newcontent);
        });
    }
}

function RemoveDraw(place) {
    $('#'+place+'poly').html('');
    $('#'+place+'cable').html('');
}

function BotTopNew(drawingID){
	
	id=parseInt(drawingID);
    switch(id) {
        case 1:
            newdrawingID=3;
            break;
        case 3:
            newdrawingID=1;
            break;
        case 6:
            newdrawingID=null;
            break;
        case 7:
            newdrawingID=8;
            break;
        case 8:
            newdrawingID=7;
            break;
        case 9:
            newdrawingID=10;
            break;
        case 10:
            newdrawingID=9;
            break;
        case 15:
            newdrawingID=16;
            break;
        case 16:
            newdrawingID=15;
            break;
        default:
            newdrawingID=drawingID;
    }
	global_TopprofileID = newdrawingID;
	return newdrawingID;
}

var DrawingSize = $('#PixaluxDrawing').width();
var DrawSurfacce = DrawingSize-100;
var CenterXY = DrawingSize/2+27;

function calculateRatio(a, b, c) {
    c = c || DrawSurfacce;
    var ratio = Math.min(c / a, c / b);
    return ratio;
}

function draw_pixalux(ol, od, colo, ledcol, bheight, ltype ,edges, exitbt, exitlr) {
    console.log(colo);
    ledcol = ledcol || '#DAE1E5';
    colo = colo || ledcolor;
    if (ledcol=="url(#MyGradient)") ledcol = "url(#MyGradient2)";
	if (ledcol=="url(#MyWhite)") ledcol = "url(#MyWhite2)";
    
    //overall lenght
    ol = Number(ol);
    //overall depth
    od = Number(od);
    // fit in 450*450
    var ratio = calculateRatio(ol,od)

    //bottom border height (profile)
    var bh = bheight;

    // !!! Only used for Text Purpose !!!
    //internal lenght
    var il = ol - 4;
    //internal depth
    if (edges==2) {
		var id = od - (2 * bh);
	} else {
		var id = od - 2 - bh;
    }
	
    //rationalized
    rol = ol*ratio;
    rod = od*ratio;
    rbh = bh*ratio;
    Oxy = CenterXY;

    //console.log('ROD: '+rod);

    // Clockwise Square Point Definitions
    Ax = Oxy-(rol/2);
    Ay = Oxy-(rod/2);
    Bx = Oxy+(rol/2);
    By = Ay;
    Cx = Bx;
    Cy = Oxy+(rod/2);
    Dx = Ax;
    Dy = Cy;
    Ex = Ax;
    Ey = Dy-rbh;
    Fx = Bx;
    Fy = Cy-rbh;

    
    /*
    draw.rect(DrawingSize, DrawingSize).fill('white').stroke({
        width: 1,
        color: 'red'
    });

    /* Overall Rectangle */
    draw.rect(rol, rod).move(Ax, Ay).fill(ledcol).stroke({
        width: 2,
        color: '#000'
    });

    /* Profile Rectangle */
    draw.rect(rol, rbh).move(Ex, Ey).fill(colo).stroke({
        width: 2,
        color: '#000'
    });

    var dotted = draw.pattern(15, 1, function(add) {
        add.rect(10,1).fill('#333')
    })
      
    if (ltype=="dotted") {
        draw.rect(rol, 1).move(Dx, Dy-(rbh/2)).fill(dotted)
    }
    if (ltype=="full"){
        draw.line(Dx, Dy-(rbh/2), Cx, Cy-(rbh/2)).stroke({
            width: 1,
            color: '#333'
        })
    }

    /* Top Profile*/
    if (edges==2) {
        draw.rect(rol, rbh).move(Ax, Ay).fill(colo).stroke({
            width: 2,
            color: '#000'
        });

        if (ltype=="dotted") {
            draw.rect(rol, 1).move(Ax, Ay+(rbh/2)).fill(dotted)
        }
        if (ltype=="full"){
            draw.line(Ax, Ay+(rbh/2), Bx, By+(rbh/2)).stroke({
                width: 1
            })
        }
    
    }
    
    /* Labels */
    // if (rol>230){
	// 	var textarea = draw.text('ILLUMINATED AREA');
    //     textarea.move(Oxy-(textarea.length()/2), Oxy-10).font({
    //         fill: '#f06'
    //     });
    // } else {
	// 	var textarea = draw.text('ILLUMI-\r\nNATED\r\nAREA');
    //     textarea.move(Oxy-25, Oxy-30).font({
    //         fill: '#f06'
    //     });       
    // }
     if (rol>230){
		var textarea = draw.text('ILLUMINATED AREA');
        textarea.move(Oxy-(textarea.length()/2), Oxy-10).font({
            fill: '#f06'
        });
    } else {
		var textarea = draw.text('ILLUMI-\r\nNATED\r\nAREA');
        textarea.move(Oxy-25, Oxy-30).font({
            fill: '#f06'
        });       
    }

    // OL Quote
    var OLoffset=-55;
    var OLQuote = draw.text(ol + 'mm');
    OLQuote.move(Oxy - (OLQuote.length()/2), Ay+OLoffset-20).font({
        fill: '#000'
    });
    draw.line(Ax, Ay+OLoffset, Bx, By+OLoffset).stroke({
        width: 1
    })
    draw.polyline([Ax,Ay+OLoffset, Ax+10,Ay+OLoffset-5, Ax+10,Ay+OLoffset+5])
    draw.polyline([Bx,By+OLoffset, Bx-10,By+OLoffset-5, Bx-10,By+OLoffset+5])
    draw.line(Ax, Ay-5, Ax, Ay-60).stroke({
        width: 1
    })
    draw.line(Bx, By-5, Bx, By-60).stroke({
        width: 1
    })

    // IL Quote
    var ILoffset=-30;
    var ILQuote = draw.text(il + 'mm');
    ILQuote.move(Oxy - (ILQuote.length()/2), Ay+ILoffset-20).font({
        fill: '#f06'
    });
    draw.line(Ax, Ay+ILoffset, Bx, By+ILoffset).stroke({
        width: 1
    })
    draw.polyline([Ax,Ay+ILoffset, Ax+10,Ay+ILoffset-5, Ax+10,Ay+ILoffset+5])
    draw.polyline([Bx,By+ILoffset, Bx-10,By+ILoffset-5, Bx-10,By+ILoffset+5])

    if (rod>80.1){
        // OD Quote
        var ODoffset=-35;
        var ODQuote = draw.text(od + 'mm').rotate(-90);
        ODQuote.move(-Oxy+15, Ax+ODoffset-30).font({
            fill: '#000'
        });
        draw.line(Ax+ODoffset, Ay, Dx+ODoffset, Dy).stroke({
            width: 1
        })
        draw.polyline([Ax+ODoffset,Ay, Ax+ODoffset+5,Ay+10, Ax+ODoffset-5,Ay+10])
        draw.polyline([Dx+ODoffset,Dy, Dx+ODoffset+5,Dy-10, Dx+ODoffset-5,Dy-10])
        draw.line(Ax-5,Ay, Ax-40, Ay).stroke({
            width: 1
        })
        draw.line(Dx-5,Dy, Dx-40, Dy).stroke({
            width: 1
        })

        if (edges==2) {
            // ID Quote
            var IDoffset=-10;
            var IDQuote = draw.text(id + 'mm').rotate(-90);
            IDQuote.move(-Oxy+20, Ax+IDoffset-30).font({
                fill: '#f06'
            });
            draw.line(Ax+IDoffset, Ay+rbh, Ex+IDoffset, Ey).stroke({
                width: 1
            })
            draw.polyline([Ax+IDoffset,Ay+rbh, Ax+IDoffset+5,Ay+10+rbh, Ax+IDoffset-5,Ay+10+rbh])
            draw.polyline([Ex+IDoffset,Ey, Ex+IDoffset+5,Ey-10, Ex+IDoffset-5,Ey-10])
            draw.line(Ax-5,Ay+rbh, Ax-15, Ay+rbh).stroke({
                width: 1
            })
            draw.line(Ex-5,Ey, Ex-15, Ey).stroke({
                width: 1
            })
        } else {
            // ID Quote
            var IDoffset=-10;
            var IDQuote = draw.text(id + 'mm').rotate(-90);
            IDQuote.move(-Oxy+20, Ax+IDoffset-30).font({
                fill: '#f06'
            });
            draw.line(Ax+IDoffset, Ay, Ex+IDoffset, Ey).stroke({
                width: 1
            })
            draw.polyline([Ax+IDoffset,Ay, Ax+IDoffset+5,Ay+10, Ax+IDoffset-5,Ay+10])
            draw.polyline([Ex+IDoffset,Ey, Ex+IDoffset+5,Ey-10, Ex+IDoffset-5,Ey-10])
            draw.line(Ex-5,Ey, Ex-15, Ey).stroke({
                width: 1
            })
        }
    } else {
        // OD Quote
        var ODoffset=-35;
        var ODQuote=draw.text(od + 'mm').font({fill: '#000'});
        ODQuote.move(5,Ay-50);
        ODQuote.rotate(-90);
        draw.line(Ax+ODoffset, Ay-70, Dx+ODoffset, Dy).stroke({
            width: 1
        })
        draw.polyline([Ax+ODoffset,Ay, Ax+ODoffset+5,Ay-10, Ax+ODoffset-5,Ay-10])
        draw.polyline([Dx+ODoffset,Dy, Dx+ODoffset+5,Dy+10, Dx+ODoffset-5,Dy+10])
        draw.line(Ax-5,Ay, Ax-40, Ay).stroke({
            width: 1
        })
        draw.line(Dx-5,Dy, Dx-40, Dy).stroke({
            width: 1
        })

        // ID Quote
        var IDoffset=-10;
        var IDQuote=draw.text(id + 'mm').font({fill: '#f06'});
        IDQuote.move(30,Ay-50);
        IDQuote.rotate(-90);
        draw.line(Ax+IDoffset, Ay-70, Ex+IDoffset, Ey).stroke({
            width: 1
        })
        draw.polyline([Ax+IDoffset,Ay, Ax+IDoffset+5,Ay-10, Ax+IDoffset-5,Ay-10])
        draw.polyline([Ex+IDoffset,Ey, Ex+IDoffset+5,Ey+10, Ex+IDoffset-5,Ey+10])
        draw.line(Ex-5,Ey, Ex-15, Ey).stroke({
            width: 1
        })
    }

    // cable exit 
    CAx=Ax;
    CAy=Ay+rbh-22;
    CBx=Bx-22;
    CBy=By+rbh-22;
    CEx=Ex;
    CEy=Ey;
    CFx=Fx-22;
    CFy=Fy;
    var efill;
    //console.log('exitbt: '+exitbt);

    switch(exitbt){
        case "Back":
            CAx=Ax;
            CAy=Ay+rbh-22;
            CBx=Bx-22;
            CBy=By+rbh-22;
            CEx=Ex;
            CEy=Ey;
            CFx=Fx-22;
            CFy=Fy;
            efill="#ababab";
            break;
        case "Top":
            CAx=Ax;
            CAy=Ay+rbh-22;
            CBx=Bx-22;
            CBy=By+rbh-22;
            CEx=Ex;
            CEy=Ey;
            CFx=Fx-22;
            CFy=Fy;
			if (colo == '#000'){
				efill="#fff";
			}else{
				efill="black";
			}
            break;
        case "End":
            CAx=Ax-22;
            CAy=Ay+rbh-22;
            CBx=Bx;
            CBy=By+rbh-22;
            CEx=Ex-22;
            CEy=Ey;
            CFx=Fx;
            CFy=Fy;
			if (colo == '#000'){
				efill="#fff";
			}else{
				efill="black";
			}
            break;
        case "Bottom":
            CAx=Ax;
            CAy=Ay-22;
            CBx=Bx-22;
            CBy=By-22;
            CEx=Ex;
            CEy=Dy;
            CFx=Fx-22;
            CFy=Cy;
			efill="black";
            break;
    }
	
	//console.log('efill: '+efill);
    
	dcplug='M 4 2 C 2.9 2 2 2.9 2 4 L 2 22 C 2 23.1 2.9 24 4 24 L 10 24 L 10 26 L 12 26 L 12 16.40625 A 1.0001 1.0001 0 0 0 11.5 15.53125 C 10.598369 15.005299 10 14.019048 10 13 C 10 11.345455 11.345455 10 13 10 C 14.654545 10 16 11.345455 16 13 C 16 14.144444 15.404729 15.078885 14.5625 15.5 A 1.0001 1.0001 0 0 0 14 16.40625 L 14 26 L 16 26 L 16 24 L 22 24 C 23.1 24 24 23.1 24 22 L 24 4 C 24 2.9 23.1 2 22 2 L 4 2 z M 13 6 C 16.9 6 20 9.1 20 13 C 20 15.824453 18.37812 18.179304 16 19.3125 L 16 16.75 C 17.167263 15.875774 18 14.573371 18 13 C 18 10.254545 15.745455 8 13 8 C 10.254545 8 8 10.254545 8 13 C 8 14.533186 8.8382787 15.830932 10 16.75 L 10 19.3125 C 7.62188 18.179304 6 15.824453 6 13 C 6 9.1 9.1 6 13 6 z';
    if (edges==2) {
        switch (exitlr) {
            case "Left":
                draw.path(dcplug).fill(efill).move(CAx,CAy);
                break;
            case "Right":
                draw.path(dcplug).fill(efill).move(CBx,CBy);
        }
    }

    switch (exitlr) {
        case "Left":
            draw.path(dcplug).fill(efill).move(CEx,CEy);
            break;
        case "Right":
            draw.path(dcplug).fill(efill).move(CFx,CFy);
    }

}

function clear_pixalux() {
    draw.clear();
}

function update_pixalux(olength, odepth, ncolor, ledcolor, bheight, ltype, nedge, cexit, chexit) {
    clear_pixalux();
    console.log('ncolor',ncolor);
    draw.size(DrawingSize, DrawingSize);
    draw_pixalux(olength, odepth, ncolor, ledcolor, bheight, ltype, nedge, cexit, chexit);
    //console.log('\nolength: '+olength+'\nodepth: '+odepth+'\nncolor: '+ncolor+'\nledcolor: '+ledcolor+'\nbheight: '+bheight+'\nltype: '+ltype+'\nnedge: '+nedge+'\n');
}
