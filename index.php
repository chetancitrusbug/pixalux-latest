<?php session_start(); 
	function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}
	
	$ip = get_client_ip(); ?>
<!DOCTYPE html>  
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Build your Pixalux Structural Light Panel</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="all,follow">
		<!-- Bootstrap CSS-->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome CSS-->
		<link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
		<!-- Lightbox-->
		<link rel="stylesheet" href="vendor/lightbox2/css/lightbox.min.css">
		<!-- theme stylesheet-->
		<link rel="stylesheet" href="css/styleindex.default.css" id="theme-stylesheet">
		<!-- Custom stylesheet - for your changes-->
		<link rel="stylesheet" href="css/custom.css">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/favicon.ico">
		<style type="text/css">
			.highlight{
			color: #fff !important;
			background-color: #6c757d !important;
			border-color: #6c757d !important;
			}
			.error{
			color: red;
			}
			.color-highlight{
			border: 5px solid #6c757d !important;
			}
			#success-alert{
			display: none;
			}
			.border{
			border:3px solid #6c757d !important;
			}
		</style>
		<!-- Tweaks for older IEs-->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
	</head>
	<body>
		<!--<section id="intro" class="intro">
			<div class="content">
			  <div class="container">
			    <div class="row">
			      <div class="col">
			        <p>1741 Sydney Rd, Campbellfield, VIC, 3061</p>
			      </div>
			      <div class="col">
			        <p class='text-right'>1300 113 532</p>
			      </div>
			    </div>
			  </div>
			</section>
			< intro end-->
		<!-- navbar-->
		<?php include("header.php")?>
		<!-- about-->
		<section id="about" class="text" style="background-color: #dae0e5;">
			<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<?php
						if(isset($_SESSION['error']) && !empty($_SESSION['error'])){
						  foreach ($_SESSION['error'] as $value) {
						?>
					<p class="section-subtitle error"><?php echo $value; ?></p>
					<?php
						}
						}
						unset($_SESSION['error']);
						?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h1>Build your Pixalux Structural Light Panel</h1>
					<p class="section-subtitle">
						Specify the features of your Pixalux Structural Light Panel using the form below.<br>For volume pricing and special orders, talk to the team directly.
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<!-- id="create-product-form" -->
					
					<?php
						if(isset($_SESSION['pincode']['data'])){
							$pincode = $_SESSION['pincode']['data'];
						}

						if(isset($_GET['id']) && isset($_SESSION[$ip][$_GET['id']])){
						  $sessionData = $_SESSION[$ip][$_GET['id']];
						  
						  // echo "<pre>"; print_r($sessionData); exit();
						}
						?>
					<form action="cart.php" method="POST" id="submit_form">
						<input type="hidden" name="key" value="<?php echo isset($_GET['id'])?$_GET['id']:null; ?>">
						<input type="hidden" name="led_name" value="<?php echo isset($sessionData['led_name'])?$sessionData['led_name']:null ?>">
						<input type="hidden" name="lit_name" value="<?php echo isset($sessionData['lit_name'])?$sessionData['lit_name']:null ?>">
						<input type="hidden" name="profile_name" value="<?php echo isset($sessionData['profile_name'])?$sessionData['profile_name']:null ?>">
						<input type="hidden" name="cable_side" value="<?php echo isset($sessionData['cable_side'])?$sessionData['cable_side']:null ?>">
						<input type="hidden" name="cable_exit" value="<?php echo isset($sessionData['cable_exit'])?$sessionData['cable_exit']:null ?>">
						<span class="led_id" data-id="<?php echo isset($sessionData['led'])?$sessionData['led']:null ?>">
						<span class="side_id" data-id="<?php echo isset($sessionData['side'])?$sessionData['side']:null ?>">
						<span class="cexit_id" data-id="<?php echo isset($sessionData['cexit'])?$sessionData['cexit']:null ?>">
						<span class="chexit_id" data-id="<?php echo isset($sessionData['chexit'])?$sessionData['chexit']:null ?>">
						<div id="accordion">
							<div class="card bg-light">
								<div class="card-header edited" id="headingFour">
									<i class="fa fa-arrows fa-fw"></i>
									<a data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
									1. Panel Size
									</a>
								</div>
								<div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#accordion">
									<div class="card-block p-3" id="">
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="depth">Depth (mm)</label>
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text">
															<i class="fa fa-arrows-v"></i>
														</div>
													</div>
													<input type="number" class="form-control" id="depth" name="depth" min=200 max=800 placeholder="Depth" value="<?php echo isset($sessionData['depth'])?$sessionData['depth']:200 ?>">
												</div>
											</div>
											<div class="form-group col-md-6">
												<label for="length">Length (mm)</label>
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text">
															<i class="fa fa-arrows-h"></i>
														</div>
													</div>
													<input type="number" class="form-control" id="length" name="length" min=200 max=1800 placeholder="Length" value="<?php echo isset($sessionData['length'])?$sessionData['length']:200 ?>">
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-1">
												<i class="fa fa-info-circle fa-2x"></i>
											</div>
											<div class="message form-group col-md-11">
												<ul>
													<div class="message" id='mintext'>
														<li>Panels cannot be less than 200mm x 200mm.</li>
													</div>
													<div class="message" id='lsizetext'></div>
													<div class="message" id='dsizetext'></div>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card bg-light">
								<div class="card-header edited" id="headingOne">
									<i class="fa fa-lightbulb-o fa-fw"></i>
									<a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									2. LEDs
									</a>
								</div>
								<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
									<div class="card-block p-3" id="">
										<div class="form-row">
											<div class="form-group col-md-12">
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text">
															<i class="fa fa-lightbulb-o"></i>
														</div>
													</div>
													<select id="led" name="led" class="custom-select">
														<option></option>
													</select>
													<input type="text" id="colour" name="colour" value="<?php echo isset($sessionData['colour'])?$sessionData['colour']:null ?>" hidden>
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-1">
												<i class="fa fa-info-circle fa-2x"></i>
											</div>
											<div class="message form-group col-md-11">
												<ul>
													<div class="message" id='pitchtext'>Choosing the LED will set the pitch.</div>
													<div class="message" id='pitchtext2'></div>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card bg-light">
								<div class="card-header edited" id="headingTwo">
									<i class="fa fa-sun-o fa-fw"></i>
									<a data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
									3. Illuminated faces
									</a>
								</div>
								<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
									<div class="card-body">
										<div class="input-group">
											<label class="sr-only" for="side">Sides</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<div class="input-group-text">
														<i class="fa fa-lightbulb-o"></i>
													</div>
												</div>
												<select id="side" name="side" class="custom-select">
													<option></option>
												</select>
											</div>
											<div class="message" id='sidetext'>Choosing the LED will set the pitch.</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card bg-light">
								<div class="card-header edited" id="headingThree">
									<i class="fa fa-square-o fa-fw"></i>
									<a data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
									4. LED extrusion profile
									</a>
								</div>
								<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
									<div class="card-block pb-3" id="profileDrawing">
									</div>
									<div class="card-body" id='standerror'>
									</div>
								</div>
							</div>
							<div class="card bg-light">
								<div class="card-header edited" id="headingFive">
									<i class="fa fa-paint-brush fa-fw"></i>
									<a data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
									5. Finish colour
									</a>
								</div>
								<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
									<div class="card-block p-3" id="">
										<div class="form-row">
											<div class="form-group col-md-12">
												<label for="mycp">Finish colour</label>
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text">
															<i class="fa fa-paint-brush"></i>
														</div>
													</div>
													<label class="form-control mycp3" style="background-color:#fff;">Gloss white</label>
													<input type="hidden" class="form-control" id="mycp3" name="mycp" value="Gloss white" style="background-color:#fff;">
												</div>

												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text">
															<i class="fa fa-paint-brush"></i>
														</div>
													</div>
													<label class="form-control mycp1" style="background-color:#000; color:#fff;">Gloss black</label>
													<input type="hidden" class="form-control" id="mycp1" name="mycp" value="Gloss black" style="background-color:#000; color:#fff;">
												</div>
												
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text">
															<i class="fa fa-paint-brush"></i>
														</div>
													</div>
													<label class="form-control mycp2" style="background-color:#666; color:#fff;">Aluminium</label>
													<input type="hidden" class="form-control" id="mycp2" name="mycp" value="Aluminium" style="background-color:#666; color:#fff;">
												</div>
												<input type="hidden" name="frame_color" value="<?php echo isset($sessionData['frame_color'])?$sessionData['frame_color']:null ?>">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-1">
												<i class="fa fa-info-circle fa-2x"></i>
											</div>
											<div class="message form-group col-md-11">
												<ul>
													<li>The selected colour applies to the extrusion profile and the 2mm ABS edge tape applied to the short edges.</li>
													<li>Aluminium colour refers to a natural anodised profile and brushed aluminium look ABS edge.</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card bg-light">
								<div class="card-header edited" id="headingSix">
									<i class="fa fa-external-link fa-fw"></i>
									<a data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
									6. Cable Exit
									</a>
								</div>
								<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
									<div class="card-block p-3" id="">
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="cexit">Cable exit</label>
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text">
															<i class="fa fa-external-link"></i>
														</div>
													</div>
													<select id="cexit" name="cexit" class="form-control">
														<option></option>
													</select>
												</div>
											</div>
											<div class="form-group col-md-6">
												<label for="chexit">Choose Side</label>
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text">
															<i class="fa fa-external-link"></i>
														</div>
													</div>
													<select id="chexit" name="chexit" class="form-control">
														<option selected="true" disabled>Choose</option>
														<option>Left</option>
														<option>Right</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card bg-light">
								<div class="card-header" id="headingSeven">
								  <!-- <i class="<i class="far fa-list-alt"></i>fa fa-external-link fa-fw"></i> -->
								  <i class="fa fa-th-list"></i>
								  <a data-toggle="collapse" id="checkSummary" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
									7. Summary
								  </a>
								</div>
								<div id="collapseSeven" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
									<div class="card-block card-block-01 p-3" id="">
									  <div class="form-row">
										<div class="form-group col-md-12">
											<div class="product-line-grid-body col-xs-8 mid">
											  <div class="product-line-info">
												<span class="label">Size:</span>
												<span class="value"><label id="depthDetail"></label>mm D x <label id="lengthDetail"></label>mm L</span>								  
											  </div>
											  <div class="product-line-info">
												<span class="label">LEDs:</span>
												<span class="value" id="ledDetail"></span>
											  </div>
											  <div class="product-line-info">
												<span class="label">Illuminated faces:</span>
												<span class="value"  id="litDetail"></span>
											  </div>
											  <div class="product-line-info">
												<span class="label">Profile:</span>
												<span class="value" id="profileChange"></span>
											  </div>
											  <div class="product-line-info">
												<span class="label">Frame Colour:</span>
												<span class="value" id="frameColor">  </span>
											  </div>
											  <div class="product-line-info">
												<span class="label">Cable Exit:</span>
												<span class="value" ><label id="sideExit" ></label> | <label id="cableExit"></label></span>
											  </div>
											  <div class="product-line-info">
												<span class="label">Power:</span>
												<span class="value" id="powerLed">14.4 W</span>
												<input type= "hidden" id="powerled" name="power" value="" />
											  </div>
											  <div class="product-line-info">
												<span class="label">Voltage:</span>
												<span class="value" id="voltageLed" >12 V</span>
												<input type= "hidden" id="voltageled" name="voltage" value="" />
											  </div>
											  <div class="product-line-info">
												<span class="label">Price:</span>
												<span class="value" id="priceLed" style="font-weight:bold">$0.00</span>
												<input type= "hidden" id="priceLed" name="price" value="" />
											  </div>
											</div>
											<!-- <div class="col-md-12 col-xs-12 qty">
																			
												<div class="input-group bootstrap-touchspin">
													<div class="form-group">
														<div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">Qty : <input id="qty" type="number" value="1" size="2"  min="1" max="10" name="qty" class="qty col-md-8 form-control qty-0"></div>
														<label>For large orders please contact the team directly at sales@pixaluxmanufacturing.com.au or on 1300 113 532.</label>
													</div>
												</div>
												<!-- <p><a href="http://localhost/corephp/pixalux?id=0" class="btn btn-edit">Edit</a></p> ->
											</div> -->
											<div class="col-md-12 col-xs-12 ">
												<div class="input-group bootstrap-touchspin">
													<div class="form-group">
														<div class="input-group bootstrap-touchspin bootstrap-touchspin-injected qty-error">
															<span class="label-span">Qty : </span>
															<input id="qty" type="number" style="width:100%"  value="<?php echo isset($sessionData['qty']) ? $sessionData['qty'] : 1 ?>" size="2" min="1" max="10" step="1" name="qty"  required class="qty col-md-8 form-control qty-0">
														</div>
														<label class="label-p">For large orders please contact the team directly at <a href="mailto:sales@pixaluxmanufacturing.com.au">sales@pixaluxmanufacturing.com.au</a> or on <a href="tel:1300 113 532">1300 113 532</a>.</label>
													</div>
												</div>
												<!-- <p><a href="http://localhost/corephp/pixalux?id=0" class="btn btn-edit">Edit</a></p> -->
											</div>
										</div>
									  </div>
									</div>
								</div>
							</div>
						</div>
						<div class="input-group mb-2" hidden>
							<div class="form-group col-md-6">
								<label for="pitch">Pitch</label>
								<div class="input-group mb-2">
									<input class="form-control" type="text" id="pitch" name="pitch" value="<?php echo isset($sessionData['pitch'])?$sessionData['pitch']:null ?>">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="height">height</label>
								<div class="input-group mb-2">
									<input class="form-control" type="text" id="height" name="height" value="<?php echo isset($sessionData['height'])?$sessionData['height']:null ?>">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="LineType">LineType</label>
								<div class="input-group mb-2">
									<input class="form-control" type="text" id="LineType" name="LineType" value="<?php echo isset($sessionData['LineType'])?$sessionData['LineType']:null ?>">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="newlength">newlength</label>
								<div class="input-group mb-2">
									<input class="form-control" type="text" id="newlength" name="newlength" value="<?php echo isset($sessionData['newlength'])?$sessionData['newlength']:null ?>">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="newdepth">newdepth</label>
								<div class="input-group mb-2">
									<input class="form-control" type="text" id="newdepth" name="newdepth" value="<?php echo isset($sessionData['newdepth'])?$sessionData['newdepth']:null ?>">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="edge">edge</label>
								<div class="input-group mb-2">
									<input class="form-control" type="text" id="edge" name="edge" value="<?php echo isset($sessionData['edge'])?$sessionData['edge']:null ?>">
								</div>
							</div>
						</div>
						<canvas id="canvas1" style="display: none;"></canvas>
						<canvas id="canvas2" style="display: none;"></canvas>
						<canvas id="canvas3" style="display: none;"></canvas>
						<input type="hidden" name="first_part" value="<?php echo isset($sessionData['first_part'])?$sessionData['first_part']:null ?>">
						<input type="hidden" name="second_part" value="<?php echo isset($sessionData['second_part'])?$sessionData['second_part']:null ?>">
						<input type="hidden" name="third_part" value="<?php echo isset($sessionData['third_part'])?$sessionData['third_part']:null ?>">
						<div class="form-group forPincode">
							<label for="newdepth">Enter Your Post Code* (You can add Post Code once, using this calculate Freight)</label>
							<div class="input-group input-custom-1 mb-2">
							
													
								<input class="form-control" type="text" maxlength="4" minlength="4" <?php echo (isset($pincode) && $pincode != '') ? 'readonly' : '' ?> id="pincodeForCart" name="pincode" value="<?php echo (isset($pincode) && $pincode != '') ? $pincode : '' ?>">
								<?php if(isset($pincode) && $pincode != ''){ ?>
									
									<div class="input-group-prepend">
										<div class="input-group-text" id="editPincode">
											<i class="fa fa-pencil"></i>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
						<button type="submit" class="btn btn-primary cart_button">Add to Cart</button>
					</form>
				</div>
				<div class="col-md-5" id="PixaluxDrawing">
					<img src="img/3rdangle.png" class="pull-left">
					<h4 class="text-center">Size Ratio
						<br>Preview
					</h4>
					<div id="pixalux" class="third_part">
					</div>
					<canvas id="canvas1" name='first_part' style="display:none">
					<canvas id="canvas2" name='second_part' style="display:none">
					<canvas id="canvas3" name='third_part' style="display:none">
					<div class="alert alert-success" id="success-alert">
						<strong>Success! </strong>
						Product is added to your cart.
					</div>
				</div>
				<div class="col-md-3">
					<h4 class="text-center" style=" margin-bottom:28px">Profile Preview</h4>
					<div id="svgCanvas" style="border: 1px solid black;">
						<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"  viewBox="0 40 275 200" id="first_part" class="first_part">
							<g id="profileCutTop" transform="translate(272 280) rotate(180)">
								<g id="topledcolor"></g>
								<g id="topbase"></g>
								<g id="toppoly"></g>
								<g id="topcable"></g>
							</g>
						</svg>
						<svg id="profileCutBottom" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="0 40 275 200" class="second_part">
							<g id="botledcolor"></g>
							<g id="botbase"></g>
							<g id="botpoly"></g>
							<g id="botcable"></g>
						</svg>
					</div>
				</div>
			</div>
		</section>
		<?php include("footer.php")?>
		<!--<footer style="background-color: #98999A;">
			<div class="container">
				<div class="row copyright">
					<div class="col-md-6">
						<p class="mb-md-0 text-center text-md-left">&copy;2018 PIXALUX MANUFACTURING AUSTRLIA</p>
						<p class="mb-md-0 text-center text-md-left">BROWSER CAPATIBLE WITH <i class="fa fa-chrome"></i> <i class="fa fa-firefox"></i></p>
					</div>
					<div class="col-md-6">
						<p class="credit mb-md-0 text-center text-md-right">Created by
							<a href="https://www.Leapfrogmarket.com.au">Leapfrog Market</a>
						</p>
					</div>
				</div>
			</div>
		</footer> -->
		<!-- JavaScript files-->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/popper.js/umd/popper.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/jquery.cookie/jquery.cookie.js"></script>
		<script src="vendor/jquery.cookie/jquery.cookie.js"></script>
		<script src="vendor/lightbox2/js/lightbox.min.js"></script>
		
		<script src="js/front.js"></script>
		<script src="js/svg.js"></script>
		<script src="app/app.js"></script>
		<script src="app/designs/read-designs.js"></script>
		<script src="app/products/create-product.js"></script>
		<!-- <script src="app/home.js"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/dom-to-image/2.6.0/dom-to-image.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
		<script src="app/home_new.js"></script>
	</body>
</html>