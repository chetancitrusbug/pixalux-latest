<?php session_start();
function get_client_ip()
{
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if (isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if (isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	else if (isset($_SERVER['REMOTE_ADDR']))
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = 'UNKNOWN';
	return $ipaddress;
}

$ip = get_client_ip();


?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Checkout : Black And White Bootstrap Landing Page / Portfolio</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/font-awesome/css/font-awesome.min.css">
  <!-- Lightbox-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/vendor/lightbox2/css/lightbox.min.css">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="http://pixalux.totalsimplicity.com.au/css/custom.css">
  <!-- Favicon-->
  <link rel="shortcut icon" href="http://pixalux.totalsimplicity.com.au/img/favicon.ico">
  <!-- Tweaks for older IEs-->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>
	<?php
		// $_SESSION['POST'] = $_POST;
		include_once 'api/config/database.php';
		// get database connection
		// date_default_timezone_set('Asia/Kolkata');
		// echo "<pre>"; print_r('here'); exit();
		if(!empty($_POST)){
			$_SESSION['cart'] = $_POST;
			$db = new Database();
			// Create connection
			if (strpos(gethostname(), '.local') !== false) {
				$conn = new mysqli($db->host, $db->username, $db->password, $db->db_name);
			}else{
				$conn = new mysqli($db->host, $db->usernameLocal, $db->passwordLocal, $db->db_name);
			}
		

			// Check connection
			if ($conn->connect_error) {
			    die("Connection failed: " . $conn->connect_error);
			} 

			
			
			$conn->close();
		}
		// echo "<pre>"; print_r($_SESSION); exit();
	?>
  <!-- navbar-->
	<?php include("header.php") ?>
  <!-- <header class="header">
    <nav class="navbar navbar-expand-lg">
      <div class="container">
        <a href="#intro" class="navbar-brand link-scroll">
          <img src="http://pixalux.totalsimplicity.com.au/img/logo.png" alt="" class="img-fluid">
        </a>
        <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
          aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right">
          <i class="fa fa-bars"></i>
        </button>
        <div id="navbarSupportedContent" class="collapse navbar-collapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="#intro" class="nav-link link-scroll">Home</a>
            </li>
            <li class="nav-item">
              <a href="#about" class="nav-link link-scroll">About </a>
            </li>
            <li class="nav-item">
              <a href="#services" class="nav-link link-scroll">Services</a>
            </li>
            <li class="nav-item">
              <a href="#portfolio" class="nav-link link-scroll">Portfolio</a>
            </li>
            <li class="nav-item">
              <a href="#text" class="nav-link link-scroll">Text</a>
            </li>
            <li class="nav-item">
              <a href="#contact" class="nav-link link-scroll">Contact</a>
            </li>
            <li class="nav-item">
              <a href="cart.php" class="nav-link link-scroll"><img src="cart.png" height="30" width="30"></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header> -->
  
   <!-- checkout-->
  <section id="checkout" class="text clearfix">
	<div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1>Checkout</h1>
        </div>
      </div>
	  
	  <div class="row">
	  <div class="col-md-8 left">
		
		
	  
		<section class="checkout-step -unreachable" id="checkout-addresses-step">
		<h1 class="step-title">Addresses</h1>
			
			<div class="js-address-form">
				<form method="POST" action="payment.php">
				<p>The selected address will be used both as your personal address (for invoice) and as your delivery address.</p>
				
				<div id="delivery-address">
					<div class="js-address-form">
						<div class="form-fields">
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">First name</label>
								<div class="col-md-6"><input class="form-control" name="firstname" value="" maxlength="32" required="" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">Last name</label>
								<div class="col-md-6"><input class="form-control" name="lastname" value="" maxlength="32" required="" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">Email</label>
								<div class="col-md-6"><input class="form-control" name="email" value="" maxlength="128" required="" type="email"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">Address</label>
								<div class="col-md-6"><input class="form-control" name="address1" value="" maxlength="128" required="" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label">Address Complement *</label>
								<div class="col-md-6"><input class="form-control" name="address2" value="" maxlength="128" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">City</label>
								<div class="col-md-6"><input class="form-control" name="city" value="" maxlength="64" required="" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">State</label>
								<div class="col-md-6">
									<select class="form-control form-control-select" name="state_id" required="">
									<option value="" disabled="" selected="">-- please choose --</option>
									  <option value="Australian Capital Territory">Australian Capital Territory</option>
										<option value="New South Wales">New South Wales</option>
										<option value="Victoria">Victoria</option>
										<option value="Queensland">Queensland</option>
										<option value="South Australia">South Australia</option>
										<option value="Western Australia">Western Australia</option>
										<option value="Tasmania">Tasmania</option>
										<option value="Northern Territory">Northern Territory</option>
								  </select>
								</div>
								<div class="col-md-3 form-control-comment"></div>
						</div>
						
						<div class="form-group row ">
						<?php $pincode = $_SESSION['pincode']['data']; ?>
							<label class="col-md-3 form-control-label required">Zip/Postal Code</label>
							<div class="col-md-6"><input class="form-control" <?php echo (isset($pincode) && $pincode != '') ? 'readonly' : '' ?> name="postcode" value="<?php echo (isset($pincode) && $pincode != '') ? $pincode : '' ?>" maxlength="12" required="" type="text"></div>
							<div class="col-md-3 form-control-comment"></div>
						</div>
						
						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">Country</label>
							<div class="col-md-6">
								<label class="form-control form-control-select js-country" style="border:0px !important" >Australia</label>
								<input type="hidden" name="country_id" value="Australia" />
								<!-- <select class="form-control form-control-select js-country" name="country_id" required="">
								<option value="" disabled="" selected="">-- please choose --</option>
									  <option value="8">Australia</option>
									  <option value="1">Germany</option>
									  <option value="10">Italy</option>
									  <option value="15">Portugal</option>
									  <option value="21">United States</option>
								  </select> -->
							</div>
							<div class="col-md-3 form-control-comment"></div>
						</div>
						
						<div class="form-group row ">
							<label class="col-md-3 form-control-label">Phone *</label>
							<div class="col-md-6"><input class="form-control" name="phone" value="" maxlength="32" type="text"></div>
							<!-- <div class="col-md-3 form-control-comment"></div> -->
						</div>
						
						<div class="form-group row">
							<div class="col-md-9 col-md-offset-3">
							<input id="useForInvoice" name="use_for_invoice" value="1" type="checkbox">
							<label for="useForInvoice">Use another address for invoice</label>
							</div>
						</div>

						<div id="CheckboxUncheckedAddress">
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">Address</label>
								<div class="col-md-6"><input class="form-control" name="invoiceaddress1" value="" maxlength="128"  type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label">Address Complement *</label>
								<div class="col-md-6"><input class="form-control" name="invoiceaddress2" value="" maxlength="128" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>
							
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">City</label>
								<div class="col-md-6"><input class="form-control" name="invoicecity" value="" maxlength="64" type="text"></div>
								<div class="col-md-3 form-control-comment"></div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">State</label>
								<div class="col-md-6">
									<select class="form-control form-control-select" name="invoicestate_id" >
									<option value="" disabled="" selected="">-- please choose --</option>
									 	<option value="Australian Capital Territory">Australian Capital Territory</option>
										<option value="New South Wales">New South Wales</option>
										<option value="Victoria">Victoria</option>
										<option value="Queensland">Queensland</option>
										<option value="South Australia">South Australia</option>
										<option value="Western Australia">Western Australia</option>
										<option value="Tasmania">Tasmania</option>
										<option value="Northern Territory">Northern Territory</option>
								  </select>
								</div>
								<div class="col-md-3 form-control-comment"></div>
						</div>
						
						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">Zip/Postal Code</label>
							<div class="col-md-6"><input class="form-control" name="invoicepostcode" value="" maxlength="12"  type="text"></div>
							<div class="col-md-3 form-control-comment"></div>
						</div>
						
						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">Country</label>
							<div class="col-md-6">
								<label class="form-control form-control-select js-country" >Australia</label>
								<input type="hidden" name="invoicecountry_id" value="Australia" />
								<!-- <select class="form-control form-control-select js-country" name="invoicecountry_id" >
								<option value="" disabled="" selected="">-- please choose --</option>
									  <option value="8">France</option>
									  <option value="1">Germany</option>
									  <option value="10">Italy</option>
									  <option value="15">Portugal</option>
									  <option value="21">United States</option>
								  </select> -->
							</div>
							<div class="col-md-3 form-control-comment"></div>
						</div>
						</div>
						
				</div>
				
				<footer class="form-footer clearfix">
					<button type="submit" class="continue btn btn-primary button-small pull-xs-right button-small" name="confirm-addresses" value="1">
					  Continue
				  </button>
				  <a class="js-cancel-address cancel-address btn btn-primary button-small pull-xs-right button-small" href="cart.php">Cancel</a>
				</footer>
				
				
			</div>
			</div>
			
			</form>
				  
				  
				  
				
			  </div>
		
		</section>
		
	  
	  </div><!-- end of col -->
	  <div class="col-md-4 right">
		
		<div class="right-box">
					<section id="js-checkout-summary" class="card js-cart" data-refresh-url="#">
					  <div class="card-block">
						<div class="cart-summary-products">
							<p><?php echo count($_SESSION[$_SESSION['ip']])?> item</p>
							<!-- <p><a href="#" data-toggle="collapse" data-target="#cart-summary-product-list">show details</a></p> -->
							<div class="collapse" id="cart-summary-product-list">
							<ul class="media-list clearfix">
							<li class="media  clearfix">
								<div class="media-left">
								<a href="#" title="Product Name"><img class="media-object" src="product-img.png" class="img-fluid" alt="Product Name"></a>
								</div>
								<div class="media-body">
									<span class="product-name">Product Name</span>
									<span class="product-quantity">x2</span>
									<span class="product-price float-xs-right">$28.72</span>
								</div>
							</li>
							</ul>
							</div>
						</div>
						
						<div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-products">
							<span class="label">Subtotal</span>
							<span class="value pull-right">$<?php echo isset($_SESSION['cart']['item_price'])?$_SESSION['cart']['item_price']:'0'; ?></span>
						</div>
						
						<div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-shipping">
							<span class="label">Shipping to <?php echo $_SESSION['pincode']['data'] ?></span>
							<span class="value pull-right">$<?php echo isset($_SESSION['cart']['shipping'])?$_SESSION['cart']['shipping']:0; ?></span>
						</div>
						
					</div>
					
					<div class="block-promo">
						<div class="cart-voucher">
						<p>
						<!-- <a class="collapse-button promo-code-button" data-toggle="collapse" href="#promo-code" aria-expanded="false" aria-controls="promo-code">
						Have a promo code?
						</a> -->
						</p>
						
						<div class="promo-code collapse" id="promo-code">
							<form action="#" data-link-action="add-voucher" method="post">
								<input class="form-control promo-input" name="discount_name" placeholder="Promo code" type="text">
								<button type="submit" class="btn btn-primary"><span>Add</span></button>
							</form>
							
						</div>
					</div>
				</div>
				
				<hr>
				
				<div class="card-block cart-summary-totals">
					<div class="cart-summary-line cart-total">
						<span class="label">Sub Total (tax excl.)</span>
						<span class="value pull-right">$<?php echo isset($_SESSION['cart']['total_price'])?$_SESSION['cart']['total_price']:0; ?></span>
					</div>
					
					<div class="cart-summary-line">
						<span class="label sub">Taxes</span>
						<span class="value sub pull-right">$<?php echo isset($_SESSION['cart']['total_tax']) ? $_SESSION['cart']['total_tax'] : 0; ?></span>
					</div>
					<hr>
					<div class="cart-summary-line">
						<span class="label sub">Total (tax incl.)</span>
						<span class="value sub pull-right">$<?php echo isset($_SESSION['cart']['total_price_with_tax']) ? $_SESSION['cart']['total_price_with_tax'] : 0; ?></span>
					</div>
				</div>
				
				<hr>
				
				
				<div id="block-reassurance">
					<ul>
						<li>
							<div class="block-reassurance-item">
							<!-- <span class="reassurance-img">
								<img src="ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)">
							</span> -->
							<span class="reassurance-img icon-20">
									<img src="img/terms_and_condition.png" alt="TERMS OF SERVICE">
								</span>
							<span><a href="https://static1.squarespace.com/static/557e2414e4b00283cf24caf9/t/5bd114247817f75a062fb3ff/1540428839516/Terms+Of+Service+-+Pixalux+Manufacturing+Australia.pdf">TERMS OF SERVICE</a></span>
							</div>
						</li>
						<li>
							<div class="block-reassurance-item">
								<span class="reassurance-img">
									<img src="img/ic_verified_user_black_36dp_1x.png" alt="PRIVACY POLICY (edit with Customer reassurance module)">
								</span>
								<span><a href="https://www.pixaluxmanufacturing.com.au/s/Privacy-Policy-Pixalux-Manufacturing-Australia.pdf">PRIVACY POLICY</a></span>
							</div>
						</li>
						<li>
							<div class="block-reassurance-item">
							<span class="reassurance-img">
								<img src="img/ic_swap_horiz_black_36dp_1x.png" alt="REFUNDS & RETURNS (edit with Customer reassurance module)">
							</span>
							<span><a href="https://www.pixaluxmanufacturing.com.au/s/Refunds-Return-Policy-Pixalux-Manufacturing-Australia.pdf">REFUNDS & RETURNS</a></span>
							</div>
						</li>
						<li>
							<div class="block-reassurance-item">
								<span class="reassurance-img">
									<img src="img/ic_local_shipping_black_36dp_1x.png" alt="SHIPPING POLICY (edit with Customer reassurance module)">
								</span>
								<span><a href="https://www.pixaluxmanufacturing.com.au/s/Shipping-Policy-Pixalux-Manufacturing-Australia.pdf">SHIPPING POLICY</a></span>
							</div>
						</li>
					</ul>
			  </div><!-- end of block-reassurance -->
				
				</section>
				
				
				
		</div>
		
	  </div><!-- end of col -->
	  
	  </div><!-- end of row -->
	 

	 </div><!-- end of container -->
  
  </section>
  
  <?php include("footer.php")?>
		<!--<footer style="background-color: #98999A;">
			<div class="container">
				<div class="row copyright">
					<div class="col-md-6">
						<p class="mb-md-0 text-center text-md-left">&copy;2018 PIXALUX MANUFACTURING AUSTRLIA</p>
						<p class="mb-md-0 text-center text-md-left">BROWSER CAPATIBLE WITH <i class="fa fa-chrome"></i> <i class="fa fa-firefox"></i></p>
					</div>
					<div class="col-md-6">
						<p class="credit mb-md-0 text-center text-md-right">Created by
							<a href="https://www.Leapfrogmarket.com.au">Leapfrog Market</a>
						</p>
					</div>
				</div>
			</div>
		</footer> -->
  <!-- JavaScript files-->
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery/jquery.min.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/popper.js/umd/popper.min.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery.cookie/jquery.cookie.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery.cookie/jquery.cookie.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/lightbox2/js/lightbox.min.js"></script>
  </script>
  <script src="js/front.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/js/svg.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/app.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/designs/read-designs.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/products/create-product.js"></script>
  
  
  <link href="css/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" media="all">
   <script src="js/jquery.bootstrap-touchspin.js"></script>
  
   <script>$('#CheckboxUncheckedAddress').hide()
            $("input[name='qty']").TouchSpin({
                min: 0,
                max: 1000,
                stepinterval: 50,
                maxboostedstep: 100
						});
						$('input[name="use_for_invoice"]').click(function(){
							if(($(this). prop("checked") == true)){
								$('#CheckboxUncheckedAddress').show()
							}else{
								$('#CheckboxUncheckedAddress').hide()
							}

						});
        </script>
  
</body>

</html>