<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/drawing.php';
 
// instantiate database and drawing object
$database = new Database();
$db = $database->getConnection();
 
$pincode = $_GET['postcode'];
			// query to read single record
$query = "SELECT * FROM freight_price_range d	WHERE d.min_postcode <= :post and d.max_postcode >= :post  LIMIT	0,1";
$stmt = $db->prepare($query);
$stmt->bindValue(':post', $pincode, PDO::PARAM_STR);
$stmt->execute();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
    echo json_encode(
        array("status" => true)
    );
    
}
 
else{
    echo json_encode(
        array("status" => false)
    );
}
?>
