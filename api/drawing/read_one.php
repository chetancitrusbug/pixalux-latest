<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/drawing.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare drawing object
$drawing = new drawing($db);
 
// set ID property of drawing to be edited
$drawing->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of drawing to be edited
$drawing->readOne();
 
// create array
$arr = array(
    "id" =>  $drawing->id,
    "name" => $drawing->name,
    "value" => json_decode($drawing->value),
    "content" => $drawing->content,
    "type" => $drawing->type);
 
// make it json format
print_r(json_encode($arr));
?>
