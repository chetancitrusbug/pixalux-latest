<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/drawing.php';
 
// instantiate database and drawing object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
// $drawing = new drawing($db);
 
// // get keywords
$depth =isset($_GET["depth"]) ? $_GET["depth"] : "";
$led =isset($_GET["led"]) ? $_GET["led"] : "";
 
// // query drawings
// $stmt = $drawing->readType($keywords);
// $num = $stmt->rowCount();
// select all query
$query = "SELECT * FROM led_pricing d WHERE  d.depth >= '" . $depth . "'  ORDER BY d.depth ASC LIMIT 0,1";
    
        // prepare query statement
$stmt = $db->prepare($query);
    
        // execute query
$stmt->execute();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
 
    // drawings array
    $drawings_arr=array();
    $drawings_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
        if ($led == 53) {
            $power = $row['adjustable_power'];
			$price = $row['ww_cw'];
        } else if ($led == 54) {
            $power = $row['white_power'];
			$price = $row['white'];
        }else	if ($led == 55) {
            $power = $row['rgb_power'];
			$price = $row['rgb'];
        } else if ($led == 56) {
            $power = $row['adjustable_power'];
			$price = $row['ww_cw'];
        } else if ($led == 57) {
            $power = $row['adjustable_power'];
			$price = $row['adjustable'];
        }
        $drawings_arr["records"] =array(
            "id" => $id,
            "depth" => $depth,
            "power" => $power,
            "price" => $price,
			
        );
 
    }
 
    echo json_encode($drawings_arr);
}
 
else{
    echo json_encode(
        array("message" => "No drawings found.")
    );
}
?>
