<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate product object
include_once '../objects/product.php';
 
$database = new Database();
$db = $database->getConnection();
 
$product = new Product($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// set product property values
$product->session_id = session_id();
$product->lenght = $data->lenght;
$product->depth = $data->depth;
$product->edge_color = $data->edge_color;
$product->Line_type = $data->Line_type;
$product->led_color = $data->led_color;
$product->number_edge = $data->number_edge;
$product->exit_topbottom = $data->exit_topbottom;
$product->exit_leftrtight = $data->exit_leftrtight;
$product->border_height = $data->border_height;
$product->led_pitch = $data->led_pitch;
$product->led_id = $data->led_id;
$product->botside_id = $data->botside_id;
$product->botprofile_id = $data->botprofile_id;
$product->topside_id = $data->topside_id;
$product->topprofile_id = $data->topprofile_id;
$product->cable_id = $data->cable_id;

// create the product
if($product->create()){
    echo '{';
        echo '"message": "Product was created."';
    echo '}';
}
 
// if unable to create the product, tell the user
else{
    echo '{';
        echo '"message": "Unable to create product."';
    echo '}';
}
?>