<style>
nav.navbar a.nav-link {
    background: #fff !important;
    border: none!important;
}
ul.navbar-nav.ml-auto {
    position: relative;
}
.navbar-expand-lg .navbar-nav .dropdown-menu {
    position: absolute;
    min-width: 200px!important;
    padding: 5px 0;
    margin-left: 10px;
}
.navbar-expand-lg .navbar-nav .dropdown-menu li {
    padding: 6px 10px;
}
li.nav-item {
    position: relative;
}
.nav-item .dropdown-toggle::after {
    display: none;
}
.nav-item:hover .dropdown-menu {
    display: block;
    margin-top: 0; // remove the gap so it doesn't close
 }
</style>
<header class="header">
    <nav class="navbar navbar-expand-lg">
      <div class="container">
        <a href="/" class="navbar-brand link-scroll">
          <img src="img/logo.png" alt="" class="img-fluid">
        </a>
        <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
          aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right">
          <i class="fa fa-bars"></i>
        </button>
        <div id="navbarSupportedContent" class="collapse navbar-collapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="/" class="nav-link link-scroll">Home</a>
            </li>
            <li class="nav-item dropdown">
              <!-- <a aria-haspopup="true" href="#" onclick="return false;">Pixalux®</a> -->
              <a href=""class=" nav-link link-scroll dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  Pixalux®
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="//www.pixaluxmanufacturing.com.au">Product</a></li>
                <li><a href="//www.pixaluxmanufacturing.com.au/how">How does Pixalux® work?</a></li>
                <li><a href="//www.pixaluxmanufacturing.com.au/features">Features</a></li>
                <li><a href="//www.pixaluxmanufacturing.com.au/faqs">FAQs</a></li>
                <li><a href="//www.pixaluxmanufacturing.com.au/specs">Downloads</a></li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="//www.pixaluxmanufacturing.com.au/glowpro" class="nav-link link-scroll">Glowpro®</a>
            </li>
            <li class="nav-item">
              <!-- <a aria-haspopup="true" href="#" onclick="return false;">Pixalux®</a> -->
              <a href="" class=" nav-link link-scroll dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Applications
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <li><a href="//www.pixaluxmanufacturing.com.au/projects">Projects</a></li>
                <li><a href="//www.pixaluxmanufacturing.com.au/signage">Signage</a></li>
                <li><a href="//www.pixaluxmanufacturing.com.au/shelf-lighting">Shelf Lighting</a></li>
                <li><a href="//www.pixaluxmanufacturing.com.au/cabinet-lighting">Cabinet Lighting</a></li>
                
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link link-scroll" href="//www.pixaluxmanufacturing.com.au/blog">Blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link link-scroll" href="//www.pixaluxmanufacturing.com.au/contact">Contact</a>
            </li>
			
            <li class="nav-item">
              <a href="cart.php" class="nav-link link-scroll"><img src="img/cart.png" height="30" width="30">
                <span id="cartCount">
                  <?php if(isset($_SESSION[$ip]) && count($_SESSION[$ip]) > 0) {
                    $qty = 0;
                    foreach ($_SESSION[$ip] as $key => $value) {
                      $qty = $qty + $value['qty'];
                    }
                    echo $qty;
                  } else{
                    echo 0;
                  }
                  ?>
                </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

<!--
  <ul>
    
      

    <li class="folder-collection folder">

      

        <a aria-haspopup="true" href="#" onclick="return false;">Pixalux®</a>
        <div class="subnav">
          <ul>
            
              
                <li class="page-collection">
                  <a href="/">Product</a>
                </li>
              
              
            
              
                <li class="page-collection">
                  <a href="/how/">How does Pixalux® work?</a>
                </li>
              
              
            
              
                <li class="page-collection">
                  <a href="/features/">Features</a>
                </li>
              
              
            
              
                <li class="page-collection">
                  <a href="/faqs/">FAQs</a>
                </li>
              
              
            
              
                <li class="page-collection">
                  <a href="/specs/">Downloads</a>
                </li>
              
              
            
          </ul>
        </div>

      

    </li>



    <li class="page-collection">

      

        
          <a href="/glowpro/">Glowpro®</a>
        

        


      

    </li>



    <li class="folder-collection folder">

      

        <a aria-haspopup="true" href="#" onclick="return false;">Applications</a>
        <div class="subnav">
          <ul>
            
              
                <li class="page-collection">
                  <a href="/projects/">Projects</a>
                </li>
              
              
            
              
                <li class="page-collection">
                  <a href="/signage/">Signage</a>
                </li>
              
              
            
              
                <li class="page-collection">
                  <a href="/shelf-lighting/">Shelf Lighting</a>
                </li>
              
              
            
              
                <li class="page-collection">
                  <a href="/cabinet-lighting/">Cabinet Lighting</a>
                </li>
              
              
            
          </ul>
        </div>

      

    </li>



    <li class="blog-collection">

      

        
          <a href="/blog/">Blog</a>
        

        


      

    </li>



    <li class="page-collection">

      

        
          <a href="/contact/">Contact</a>
        

        


      

    </li>




</ul>
<div class="page-divider"></div> -->