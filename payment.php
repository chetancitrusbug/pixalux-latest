<?php session_start(); 

    include_once 'api/config/database.php';

    if(!empty($_POST)){
        $db = new Database();
        $enableSandbox = 1;
        $paypalUrl = $enableSandbox ? 'https://www.sandbox.paypal.com/cgi-bin/webscr' : 'https://www.paypal.com/cgi-bin/webscr';
   
        $cart = $_SESSION['cart'];
   
        if (strpos(gethostname(), '.local') !== false) {
            $conn = new mysqli($db->host, $db->username, $db->password, $db->db_name);
        } else {
            $conn = new mysqli($db->host, $db->usernameLocal, $db->passwordLocal, $db->db_name);
        }
        $_POST['use_for_invoice'] = isset($_POST['use_for_invoice'])?$_POST['use_for_invoice']:0;


        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
        
        $_SESSION['client_email_id'] = $_POST['email'];
        $_SESSION['user_detail'] = $_POST;
        $sql = "INSERT INTO
                    customer_detail (first_name,last_name,email,address1,address2,city,state_id,postcode,country_id,phone,use_for_invoice,deleted,created_at)
                VALUES
                    ('".$_POST['firstname']."','".$_POST['lastname']."','".$_POST['email']."','".$_POST['address1']."','".$_POST['address2']."','".$_POST['city']."','".$_POST['state_id']."','".$_POST['postcode']."','".$_POST['country_id']."','".$_POST['phone']."','".$_POST['use_for_invoice']."','0',now())";

        if ($conn->query($sql) === TRUE) {
        $user_id = mysqli_insert_id($conn);
            if($_POST['use_for_invoice']){
               
                $sql2 = "INSERT INTO
                    invoice_address (user_id,address1,address2,city,state_id,postcode,country_id)
                VALUES
                    ('".$user_id."','".$_POST['invoiceaddress1']."','".$_POST['invoiceaddress2']."','".$_POST['invoicecity']."','".$_POST['invoicestate_id']."','".$_POST['invoicepostcode']."','".$_POST['invoicecountry_id']."')";
                $conn->query($sql2);
            }
            $_SESSION['client_mail'] = 1;
        $paypalConfig = [
            'email' => 'wikitudedeveloper-facilitator@gmail.com',
            'return_url' => 'http://pixalux.totalsimplicity.com.au/thankyou.php?userId='. $user_id,
            'cancel_url' => 'http://pixalux.totalsimplicity.com.au/thankyou.php?userId=' . $user_id,
            'notify_url' => 'http://pixalux.totalsimplicity.com.au/thankyou.php?userId=' . $user_id
        ];
            $sql = "INSERT INTO
                            cart (userId,item,item_price,shipping,total_price,deleted,created_at)
                        VALUES
                            (". $user_id ."," . $cart['item'] . ",'" . $cart['item_price'] . "','" . $cart['shipping'] . "','" . $cart['total_price_with_tax'] . "','0',now())";

        
            $lastId = $conn->insert_id;
            $cart['created_at'] = date('F d, Y H:i:s A', time());
            
            if (!empty($_SESSION[$_SESSION['ip']])) {
                foreach ($_SESSION[$_SESSION['ip']] as $key => $value) {
                    if($value['SKU'] == ''){
                    $sqlItem = "INSERT INTO
					                    cart_items (cart_id,led_name,lit_name,profile_name,cable_side,cable_exit,length,depth,led,colour,side,mycp,cexit,chexit,pitch,height,LineType,newlength,newdepth,edge,qty,price,deleted,created_at)
					                VALUES
                                        (" . $lastId . ",'" . $value['led_name'] . "','" . $value['lit_name'] . "','" . $value['profile_name'] . "','" . $value['cable_side'] . "','" . $value['cable_exit'] . "','" . $value['length'] . "','" . $value['depth'] . "','" . $value['led'] . "','" . $value['colour'] . "','" . $value['side'] . "','" . $value['mycp'] . "','" . $value['cexit'] . "','" . $value['chexit'] . "','" . $value['pitch'] . "','" . $value['height'] . "','" . $value['LineType'] . "','" . $value['newlength'] . "','" . $value['newdepth'] . "','" . $value['edge'] . "','" . $cart['qty'][$key] . "','" . $cart['price'][$key] . "','0',now())";
                    }else{
                    $sqlItem = "INSERT INTO
					                    cart_items (cart_id,qty,price,catalogue_id,catalogue_sku,deleted,created_at)
					                VALUES
                                        (" . $lastId . ",'" . $cart['qty'][$key] . "','" . $cart['price'][$key] . "','". $value['ID'] ."','". $value['SKU'] ."','0',now())" ;
                    }

                    $conn->query($sqlItem);
                }
                unset($_SESSION['client_mail']);
                unset($_SESSION['client_mail_send']);

            $data = array("cmd" =>"_xclick","no_note" =>"1","lc" =>"UK","bn" =>"PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest","first_name" => $_POST['firstname'],"last_name" => $_POST['lastname'],"payer_email" => $_POST['email'],"item_number" => $lastId);
            $data['business'] = $paypalConfig['email'];

    // Set the PayPal return addresses.
            $data['return'] = stripslashes($paypalConfig['return_url']);
            $data['cancel_return'] = stripslashes($paypalConfig['cancel_url']);
            $data['notify_url'] = stripslashes($paypalConfig['notify_url']);

    // Set the details about the product being purchased, including the amount
    // and currency so that these aren't overridden by the form data.
            $data['item_name'] = 'LED ';
            $data['amount'] = $cart['total_price_with_tax'];
            $data['currency_code'] = 'USD';
            $data['custom'] = $user_id;
            $queryString = http_build_query($data);

    // Redirect to paypal IPN
            header('location:' . $paypalUrl . '?' . $queryString);


              //  header('location:admin-email-template.php');
            
            
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        
        

        $conn->close();
    }
    
    // echo "<pre>"; print_r('here'); exit();
    if(isset($_SESSION['client_mail_send'])){
        if($_GET['load'] != 1){
            header('Location:thankyou.php?load=1');
        }
        session_destroy();
    }
}
        
?>