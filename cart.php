<?php session_start();

error_reporting(0);
$fixedCalculation = 1.0265;
$tax = 0.1;
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cart : Build your Pixalux Structural Light Panel</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
  <!-- Lightbox-->
  <link rel="stylesheet" href="vendor/lightbox2/css/lightbox.min.css">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="css/custom.css">
  <!-- Favicon-->
  <link rel="shortcut icon" href="img/favicon.ico">
  <!-- Tweaks for older IEs-->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>
  <!-- navbar-->
	<?php 

		
		function get_client_ip() {
			$ipaddress = '';
			if (isset($_SERVER['HTTP_CLIENT_IP']))
					$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
					$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_X_FORWARDED']))
					$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
			else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
					$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_FORWARDED']))
					$ipaddress = $_SERVER['HTTP_FORWARDED'];
			else if(isset($_SERVER['REMOTE_ADDR']))
					$ipaddress = $_SERVER['REMOTE_ADDR'];
			else
					$ipaddress = 'UNKNOWN';
			return $ipaddress;
	}

	$ip = get_client_ip();
  	
  	$_SESSION['ip'] = $ip;
  	$end = isset($_SESSION[$ip])?array_keys($_SESSION[$ip]):[];
		$index = isset($_SESSION[$ip])?end($end)+1:0;
		
		//echo "<pre>"; print_r($_POST);exit;
include_once 'api/config/database.php';
include_once 'api/objects/drawing.php';
	 
	// get database connection
$database = new Database();
$db = $database->getConnection();
$pannelInCart = false;	 
$accessoriesInCart = false;	 
	// prepare drawing object
$drawing = new drawing($db);	
		// exit();
	
  	if(!empty($_POST)){
			
			$price = 1;
			$pincode = $_POST['pincode'];
			// query to read single record
			$query = "SELECT * FROM freight_price_range d	WHERE d.min_postcode <= :post and d.max_postcode >= :post  LIMIT	0,1";
			$stmt = $db->prepare($query);
			$stmt->bindValue(':post', $pincode, PDO::PARAM_STR);
			$stmt->execute();

			// get retrieved row
			$freightRate = $stmt->fetch(PDO::FETCH_ASSOC);
			//print_r($freightRate);
  		$_SESSION['error'] = [];
  		if(!isset($_POST['led']) || empty($_POST['led'])){
  			$_SESSION['error']['led'] = 'Please select your LEDs';
  		}
  		if(!isset($_POST['side']) || empty($_POST['side'])){
  			$_SESSION['error']['side'] = 'Please choose how many Faces are lit';
  		}
  		if(empty($_POST['profile_name'])){
  			$_SESSION['error']['profile'] = 'Please Choose how many Faces are lit';
  		}
  		if(empty($_POST['frame_color'])){
  			$_SESSION['error']['frame_color'] = 'Please Choose your frame color';
  		}
  		if(!isset($_POST['cexit']) || empty($_POST['cexit'])){
  			$_SESSION['error']['cexit'] = 'Please choose your cable exit';
  		}
  		if(!isset($_POST['chexit']) || empty($_POST['chexit'])){
  			$_SESSION['error']['chexit'] = 'Please choose your cable exit';
  		}
  		if(empty($_SESSION['error'])){
				$query = "SELECT * FROM led_pricing d	";
				$stmt = $db->prepare($query);
							// $stmt->bindValue(':depth','400', PDO::PARAM_STR);
				$stmt->execute();
							//print_r($stmt);
				$ledPricing = $stmt->fetchall(PDO::FETCH_ASSOC);
				$depth = $_POST['depth'];
				$length = $_POST['length'];
				$length = $length/1000;
				$ledPricingArray = array();
				foreach ($ledPricing as $key => $value) {
					if ($depth == $value['depth']) {
						$ledPricingArray = $value;
						break;
					}
					if ($depth <= $value['depth'] && $depth > $ledPricing[$key - 1]['depth']) {
						$ledPricingArray = $value;
						break;
					}
				}
				$power = 0;
				if (!empty($ledPricingArray)) {
					if ($_POST['led'] == 53) {
						$price = $ledPricingArray['white'];
						$power = $ledPricingArray['white_power'];
					} else if ($_POST['led'] == 54) {
						$price = $ledPricingArray['ww_cw'];
						$power = $ledPricingArray['adjustable_power'];
					}else	if ($_POST['led'] == 55) {
						$price = $ledPricingArray['rgb'];
						$power = $ledPricingArray['rgb_power'];
					} else if ($_POST['led'] == 56) {
						$price = $ledPricingArray['ww_cw'];
						$power = $ledPricingArray['adjustable_power'];
					} else if ($_POST['led'] == 57) {
						$price = $ledPricingArray['adjustable'];
						$power = $ledPricingArray['adjustable_power'];
					}
					$price = $price * $length;
				}



			// echo $price;
  			//  echo "<pre>"; print_r($_POST); exit();
  			if(isset($_POST['key']) && $_POST['key'] >= 0 && $_POST['key'] != ''){
  				// $_POST['third_part'] = base64_encode($_POST['third_part']);
		  		$_POST['mycp'] = $_POST['frame_color'];
				
				if ($_POST['power'] == '') {
					$power = $power * $_POST['depth'] / 1000;
					$power = number_format($power, 2);
					$_POST['power'] = $power;
				}
				if ( $_POST['voltage'] == '') {
					$query = "SELECT
						d.id, d.name, d.value, d.content, d.type
					FROM
						drawings d
					where id = " . $_POST['led'] . "
					ORDER BY
						d.name DESC";
					$stmt = $db->prepare($query);
								// $stmt->bindValue(':depth','400', PDO::PARAM_STR);
					$stmt->execute();
								//print_r($stmt);
					$ledVoltage = $stmt->fetchall(PDO::FETCH_ASSOC);
					$valueVoltage = json_decode($ledVoltage[0]['value']);
					$_POST['voltage'] = $valueVoltage->Voltage;
						// $power = $power * $_POST['depth'] / 1000;
						// $power = number_format($power, 2);
				}
				$_SESSION[$ip][$_POST['key']] = $_POST;
				$_SESSION[$ip][$_POST['key']]['price'] = number_format($price * $_SESSION[$ip][$_POST['key']]['qty'],2);
				$_SESSION[$ip][$_POST['key']]['actual_price'] = number_format($price,2);
				//$_SESSION[$ip][$_POST['key']]['power'] = $power;
		  	}else if(!empty($_POST['led_name'])){
				  
				if ($_POST['power'] == '') {
					$power = $power * $_POST['depth'] / 1000;
					$power = number_format($power, 2);
					$_POST['power'] = $power;
				}
				if ( $_POST['voltage'] == '') {
					$query = "SELECT
						d.id, d.name, d.value, d.content, d.type
					FROM
						drawings d
					where id = " . $_POST['led'] . "
					ORDER BY
						d.name DESC";
					$stmt = $db->prepare($query);
								// $stmt->bindValue(':depth','400', PDO::PARAM_STR);
					$stmt->execute();
								//print_r($stmt);
					$ledVoltage = $stmt->fetchall(PDO::FETCH_ASSOC);
					$valueVoltage = json_decode($ledVoltage[0]['value']);
					$_POST['voltage'] = $valueVoltage->Voltage;
				}
				  // $_POST['third_part'] = base64_encode($_POST['third_part']);
				//   print_r($_POST);exit;
		  			$_POST['mycp'] = $_POST['frame_color'];
					$_SESSION[$ip][$index] = $_POST;
					$_SESSION[$ip][$index]['price'] = number_format($price * $_POST['qty'],2);
					$_SESSION[$ip][$index]['actual_price'] = number_format($price,2);
				//	$_SESSION[$ip][$index]['power'] = $power;
				}
				$_SESSION['pincode'] = $freightRate;
				$_SESSION['pincode']['data'] = $_POST['pincode'];
  		}else{
  			if(isset($_POST['key']) && $_POST['key'] >= 0 && $_POST['key'] != ''){
  				//header('http://localhost/corephp/pixalux/?id='.$_POST['key']);	
  				header('http://pixalux.totalsimplicity.com.au?id='.$_POST['key']);	
  			}else{
  				//header('http://localhost/corephp/pixalux/');	
  				header('http://pixalux.totalsimplicity.com.au');
  			}
  		}
		}
		if(isset($_GET['pincode']) && $_GET['pincode'] != ''){
			$query = "SELECT * FROM freight_price_range d	WHERE d.min_postcode <= :post and d.max_postcode >= :post  LIMIT	0,1";
			$stmt = $db->prepare($query);
			$stmt->bindValue(':post', $_GET['pincode'] , PDO::PARAM_STR);
			$stmt->execute();

			// get retrieved row
			$freightRate = $stmt->fetch(PDO::FETCH_ASSOC);
			$_SESSION['pincode'] = $freightRate;
			$_SESSION['pincode']['data'] = $_GET['pincode'];
		}
		if(isset($_GET['additional']) && $_GET['additional'] != ''){
			
			if(array_key_exists($_GET['additional'],$_SESSION['catalauge_id'])){
			
				$alredy = $_SESSION['catalauge_id'][$_GET['additional']];
				$array = $_SESSION[$ip][$alredy];
				$query = "SELECT * FROM catalogue where ID = '" . $_GET['additional'] . "'";
				$stmt = $db->prepare($query);
							//$stmt->bindValue(':post', $pincode, PDO::PARAM_STR);
				$stmt->execute();
													// get retrieved row
				$additional = $stmt->fetch(PDO::FETCH_ASSOC);

				$additionalPrice = $fixedCalculation * $additional['price'];
						//echo number_format($additionalPrice,2);exit;
				$array = $additional;
				$array['qty'] = $_GET['qty'];	

				$array['actual_price'] = number_format($additionalPrice, 2);
				$array['price'] = $_GET['qty'] * number_format($additionalPrice, 2);

				$_SESSION['catalauge'][$ip][] = $additional['SKU'];
				$_SESSION[$ip][$alredy] = $array;
			}else{
				$_SESSION['catalauge_id'][$_GET['additional']] = $index;
				$query = "SELECT * FROM catalogue where ID = '". $_GET['additional'] ."'";
				$stmt = $db->prepare($query);
					//$stmt->bindValue(':post', $pincode, PDO::PARAM_STR);
				$stmt->execute();
											// get retrieved row
				$additional= $stmt->fetch(PDO::FETCH_ASSOC);
				
				$additionalPrice = $fixedCalculation * $additional['price'];
				//echo number_format($additionalPrice,2);exit;
				$_SESSION[$ip][$index] = $additional;
				$_SESSION[$ip][$index]['qty'] = $_GET['qty'];
				
				$_SESSION[$ip][$index]['actual_price'] = number_format($additionalPrice,2);
				$_SESSION[$ip][$index]['price'] = $_GET['qty'] * number_format($additionalPrice,2);
				
				$_SESSION['catalauge'][$ip][] = $additional['SKU'];
			}
			
		}

  	// echo "<pre>"; print_r($_SESSION); exit();
  	if($_GET['load'] != 1){
	    header('Location:cart.php?load=1');
	}
	// include_once 'api/config/database.php';
	// include_once 'api/objects/drawing.php';
	 
	// // get database connection
	// $database = new Database();
	// $db = $database->getConnection();
	 
	// // prepare drawing object
	// $drawing = new drawing($db);
  ?>
  <?php include("header.php") ?>
  
   <!-- cart-->
  	<section id="cart" class="text">
    	<div class="container">
      		<div class="row">
        		<div class="col-sm-12">
          			<h1>Shopping Cart</h1>
          			<p class="section-subtitle">Below is a summary of your order. <br />Scroll down the page to select and add accessories, or select continue shopping to add additional panels</p>
        		</div>
      		</div>
      		<div class="row">
				<div class="cart-grid-body col-xs-12 col-lg-8 col-sm-12 col-md-8">
					
					<?php
					$totalPrice = 0;
					if(isset($_SESSION[$ip]) && !empty($_SESSION[$ip])){
						$i = 1;
						//echo '<pre>';print_r($_SESSION[$ip]);echo '</pre>';
						foreach ($_SESSION[$ip] as $key => $value) {
							$priceCalculated = number_format($value['actual_price'] * $value['qty'],2);
								if( $value['SKU'] == ''){
									$pannelInCart = true;	 

					?>
					
							<div class="cart-box clearfix data-<?php echo $key; ?>">
							<!-- cart products detailed -->
								<div class="cart cart-container">
									<div class="product-line-grid clearfix">
										<!--  product left content: image-->
									  <div class="product-line-grid-left col-md-4 col-xs-4 left">
											<span class="product-image media-middle" style="position: relative;">
												<div class="left">
													<img src="<?php echo $value['first_part']; ?>" class="first_image"><br/>
													
												
												</div>
												<div class="right">
													<img src="<?php echo $value['second_part']; ?>" class="second_image">
													<!-- <img src="<?php //echo $value['third_part']; ?>" class="third_image"> -->
												</div>
												<div style="clear: both;"></div>
											</span>
									  </div>
									  <!--  product left body: description -->
									  <div class="product-line-grid-body col-md-4 col-xs-8 mid">
											<div class="product-line-info">
											  <a class="product-name" href="#" data-id_customization="0">Panel Configuration #<?php echo $i;?></a>
											</div>
											<div class="product-line-info">
												<span class="label">Size:</span>
												<span class="value"><?php echo $value['depth'].'mm D x '.$value['length'].'mm L' ; ?></span>								  
											</div>
											<div class="product-line-info">
												<span class="label">LEDs:</span>
												<span class="value"><?php echo $value['led_name']; ?></span>
											</div>
											<div class="product-line-info">
												<span class="label">Illuminated faces:</span>
												<span class="value"><?php echo $value['lit_name']; ?></span>
											 </div>
											<div class="product-line-info">
												<span class="label">Profile:</span>
												<span class="value"><?php
													// set ID property of drawing to be edited
													$drawing->id = $value['profile_name'];
													$data = $drawing->readOne(true);
													echo $data['name'];
												?></span>
											</div>
											<div class="product-line-info">
												<span class="label">Frame Colour:</span>
												<span class="value"><?php
													if($value['mycp'] == '#fff'){
														echo 'Gloss white';
													}elseif($value['mycp'] == '#000'){
														echo 'Gloss black';
													}else{
														echo 'Aluminium';
													}
												?></span>
											</div>
											<div class="product-line-info">
												<span class="label">Cable Exit:</span>
												<span class="value"><?php echo $value['cable_side']; ?> | <?php echo $value['cable_exit']; ?></span>
											</div>
											<div class="product-line-info">
												<span class="label">Power:</span>
												<span class="value"><?php echo $value['power']; ?> W</span>
											</div>
											<div class="product-line-info">
												<span class="label">Voltage:</span>
												<span class="value"><?php echo $value['voltage']; ?> V</span>
											</div>
										</div>

									  	<!--  product left body: description -->
									  	<div class="product-line-grid-right product-line-actions col-md-4 col-xs-12 right price_box-<?php echo $key; ?>">
											<div class="row">
										  		<div class="col-md-12 col-xs-12">
													<div class="row">
											  			<div class="col-md-12 col-xs-12 qty">
														 	<form class="form-horizontal" role="form">
														  		<div class="input-group bootstrap-touchspin">
																	<div class="form-group">
																		<!-- <input id="qty" type="text" min=1 max=10 value="<?php echo $value['qty'];?>" data-key="<?php echo $key; ?>" data-price="<?php echo $value['price']; ?>" name="qty" class="qty col-md-8 form-control qty-<?php echo $key; ?>"> -->
																		<input id="qty" type="text" readonly value="<?php echo $value['qty']; ?>" data-price="<?php echo $value['actual_price']; ?>" data-key="<?php echo $key; ?>"  size="2" min="1" max="10" name="qty"  class="qty col-md-8 form-control qty-<?php echo $key; ?>">
																	</div>
																</div>
															</form>
													  	</div>
											  			<div class="col-md-12 col-xs-12 price">
															<span class="product-price">
																  <strong>Price : $<span class=""><?php echo number_format($value['actual_price'],2); ?></span></strong>
															</span>
															<span class="product-price">
												  				<strong>Sub Total : $<span class="price-<?php echo $key; ?>"><?php echo $priceCalculated; ?></span></strong>
												  				<span class="value"></span>
															</span>
											  			</div>
													</div>

													<div class="row">
											  			<div class="col-md-12 col-xs-12">
													<p><a href="#" class="btn btn-remove remove-from-cart" data-key="<?php echo $key; ?>">Remove</a></p>
																<p><a href="http://pixalux.totalsimplicity.com.au?id=<?php echo $key; ?>" class="btn btn-edit">Edit</a></p>
																<!-- <p><a href="http://localhost/corephp/pixalux?id=<?php echo $key; ?>" class="btn btn-edit">Edit</a></p> -->
														</div>
													</div>

									  			</div>
											</div>
								  		</div>
								  	<div class="clearfix"></div>
									</div><!-- end of product-line-grid -->
								</div><!-- end of cart cart-container -->
							</div><!-- end of cart-box -->
					<?php	
						$i++;
						}else{
							$accessoriesInCart = true;	
				$file_name = explode(',', $value['File-Name']);
				if (count($file_name) > 0) {
					$file_name = $file_name[0];
				}
					?>
							<div class="cart-box clearfix data-<?php echo $key; ?>">
							<!-- cart products detailed -->
								<div class="cart cart-container">
									<div class="product-line-grid clearfix">
										<!--  product left content: image-->
									  <div class="product-line-grid-left col-md-4 col-xs-4 left">
											<span class="product-image media-middle" style="position: relative;padding: 10px">
												<div class="left">
													<img src="img/<?php echo $file_name; ?>" class="first_image" style="width:200px"><br/>
												</div>
												<div style="clear: both;"></div>
											</span>
									  </div>
									  <!--  product left body: description -->
									  <div class="product-line-grid-body col-md-4 col-xs-8 mid">
											<div class="product-line-info">
											  <a class="product-name" href="#" data-id_customization="0"><?php echo $value['Item']; ?></a>
											</div>
										</div>

									  	<!--  product left body: description -->
									  	<div class="product-line-grid-right product-line-actions col-md-4 col-xs-12 right price_box-<?php echo $key; ?>">
											<div class="row">
										  		<div class="col-md-12 col-xs-12">
												  <div class="row">
											  			<div class="col-md-12 col-xs-12 qty">
														 	<form class="form-horizontal" role="form">
														  		<div class="input-group bootstrap-touchspin">
																	<div class="form-group">
																		<!-- <input id="qty" type="text" min=1 max=10 value="<?php echo $value['qty']; ?>" data-key="<?php echo $key; ?>" data-price="<?php echo $value['price']; ?>" name="qty" class="qty col-md-8 form-control qty-<?php echo $key; ?>"> -->
																		<input id="qty" type="text" readonly value="<?php echo $value['qty']; ?>" data-price="<?php echo $value['actual_price']; ?>" data-key="<?php echo $key; ?>"  size="2" min="1" max="10" name="qty"  class="qty accessory col-md-8 form-control qty-<?php echo $key; ?>">
																	</div>
																</div>
															</form>
													  	</div>
											  			<div class="col-md-12 col-xs-12 price">
															<span class="product-price">
																  <strong>Price : $<span class=""><?php echo $value['actual_price']; ?></span></strong>
															</span>
															<span class="product-price">
												  				<strong>Sub Total : $<span class="price-<?php echo $key; ?>"><?php echo $priceCalculated; ?></span></strong>
												  				<span class="value"></span>
															</span>
											  			</div>
													</div>
													<?php /*
													<!-- <div class="row">
														<div class="col-md-12 col-xs-12 qty">
														 	<form class="form-horizontal" role="form">
														  		<div class="input-group bootstrap-touchspin">
																	<div class="form-group">
																		<!-- <input id="qty" type="text" min=1 max=10 value="<?php echo $value['qty']; ?>" data-key="<?php echo $key; ?>" data-price="<?php echo $value['price']; ?>" name="qty" class="qty col-md-8 form-control qty-<?php echo $key; ?>"> ->
																		<input id="qty" type="text" readonly data-price="<?php echo number_format((float)$value['actual_price'], 2, '.', ''); ?>"  data-key="<?php echo $key; ?>" name="qty" class="qty col-md-8 accessory form-control qty-<?php echo $key; ?>" value="<?php echo $value['qty']; ?>">
																	</div>
																</div>
															</form>
													  	</div>
											  			
											  			<div class="col-md-12 col-xs-12 price">
														  		<span class="product-price">
																  <strong>Price : $<span class=""><?php echo $value['actual_price']; ?></span></strong>
																</span>
																<span class="product-price">
												  				<strong>Sub Total : $<span class="price-<?php echo $key; ?>"><?php echo $value['price']; ?></span></strong>
												  				<span class="value"></span>
																</span>
											  			</div>
													</div> --> */ ?>
													<div class="row">
											  			<div class="col-md-12 col-xs-12">
																<p><a href="#" class="btn btn-remove remove-from-cart" data-key="<?php echo $key; ?>">Remove</a></p>
																<!-- <p><a href="http://localhost/corephp/pixalux?id=<?php echo $key; ?>" class="btn btn-edit">Edit</a></p> -->
														</div>
													</div>
									  			</div>
											</div>
								  		</div>
								  	<div class="clearfix"></div>
									</div><!-- end of product-line-grid -->
								</div><!-- end of cart cart-container -->
							</div><!-- end of cart-box -->
					<?php 
						}
						$totalPrice	= $totalPrice + $value['price'];
					}
					}
					
					?>
				
					<div class="clearfix"><a class="btn btn-shopping" href="http://pixalux.totalsimplicity.com.au">Continue shopping</a></div>
			
				</div><!-- end of col -->
		
				<div class="cart-grid-right col-xs-12 col-lg-4 col-sm-12 col-md-4">
					<div class="right-box">
						<div class="card cart-summary">
							<div class="cart-detailed-totals clearfix">
								<form action="checkout.php" id="checkoutForm" method="POST">
									<div class="price_loop">
									<input type="hidden" name="freight_rate"	id="freight_rate" value="<?php echo $_SESSION['pincode']['freight_rate'] ?>">
									<input type="hidden" name="min_frieght" 	id="min_frieght" 	value="<?php echo $_SESSION['pincode']['min_frieght'] ?>">
									<input type="hidden" name="max_frieght" 	id="max_frieght" 	value="<?php echo $_SESSION['pincode']['max_frieght'] ?>">
									<?php
									// To get qty and price in checkout page
									if(isset($_SESSION[$ip]) && !empty($_SESSION[$ip])){
										
										foreach ($_SESSION[$ip] as $key => $value) {
									?>
									
										<input type="hidden" name="qty[<?php echo $key; ?>]" data-tag='qty' class="qty-<?php echo $key; ?>" value="<?php echo $value['qty'];?>">
										<input type="hidden" name="price[<?php echo $key; ?>]" data-tag='price' class="price-<?php echo $key; ?>" value="<?php echo $value['actual_price']; ?>">
									<?php
										}
									}
						$shipping = 0;
						if ($totalPrice > 0) {
							//				print_r($_SESSION['pincode']);
							$shipping = $totalPrice * $_SESSION['pincode']['freight_rate'] / 100;
							if ($shipping < $_SESSION['pincode']['min_frieght']) {
								$shipping = $_SESSION['pincode']['min_frieght'];
							} else if ($shipping > $_SESSION['pincode']['max_frieght']) {
								$shipping = $_SESSION['pincode']['max_frieght'];
							}
						}
						if(!($shipping > 0)){
							$shipping = 0;
						}
						// echo $totalPrice.'<br />';
						// echo $shipping .'<br />';
						
						
						$totalPriceWithShipping = $totalPrice + $shipping;
						$totalTax = $totalPriceWithShipping * $tax;
						$totalPriceWithShippingTax = $totalPriceWithShipping + $totalTax;
						// echo $totalPriceWithShipping;
									?>
									
									</div>
									<div class="card-block">
										<div class="cart-summary-line" id="cart-subtotal-products">
											<span class="label js-subtotal"><span class="total_qty_disp"><?php echo count($_SESSION[$ip])?></span> item(s)</span>
											<span class="value">$<span class="total_price_disp"><?php echo $totalPrice; ?></span></span>
											<input type="hidden" name="item" class="total_qty_disp" value="<?php echo count($_SESSION[$ip]) ?>">
											<input type="hidden" name="item_price" class="total_price_disp" value="<?php echo $totalPrice; ?>">
										</div>
										<div class="cart-summary-line" id="cart-subtotal-shipping">
											<span class="label">Shipping to <?php echo $_SESSION['pincode']['data'] ?></span>
											<?php if(empty($_SESSION['pincode'])) { ?>
												
												<input class="form-control" type="text" maxlength="4" minlength="4" id="pincodeForCart" placeholder="Enter PostCode" name="pincode" value="">
												
											<?php } else { ?>
												<span class="value">$<span class="shipping_charge"><?php echo $shipping ?></span></span>
												<input type="hidden" class="shipping_charge" name="shipping" value="<?php echo $shipping ?>">
												<div><small class="value"></small></div>
											<?php } ?>
										</div>
									</div>

									<!-- <div class="block-promo">
										<div class="cart-voucher">
											<p>
												<a class="collapse-button promo-code-button" data-toggle="collapse" href="#promo-code" aria-expanded="false" aria-controls="promo-code">Have a promo code?</a>
											</p>
								
											<div class="promo-code collapse" id="promo-code">
												 <form action="#" data-link-action="add-voucher" method="post">
											  	<input name="token" value="" type="hidden">
											  	<input name="addDiscount" value="1" type="hidden">
											  	<input class="form-control promo-input" name="discount_name" placeholder="Promo code" type="text">
											  	<button type="submit" class="btn btn-primary"><span>Add</span></button>
												</form> 
											</div>
										</div>
									</div> -->
									<hr>
									<div class="card-block">
										<div class="cart-summary-line cart-total">
										  <span class="label">Sub Total (tax excl.)</span>
										  <span class="value">$<span class="total_price_text"><?php echo $totalPriceWithShipping ?></span>
										  <input type="hidden" name="total_price" value="<?php echo $totalPriceWithShipping ?>" class="total_price_text">
										</div>

										<div class="cart-summary-line">
										  <small class="label">Taxes</small>
										  <small class="value">$<span class="total_tax">0.00</span></small>
										   <input type="hidden" name="total_tax" value="0.00" class="total_tax">
										   <input type="hidden" name="tax" value="<?php echo $tax ?>" class="tax">
										</div>
					  				</div>
									<hr>
									<div class="card-block">
										<div class="cart-summary-line cart-total">
											<span class="label">Order Total (tax incl.)</span>
											<span class="value">$<span class="total_price_with_tax"><?php echo $totalPriceWithShippingTax ?></span>
											<input type="hidden" name="total_price_with_tax" value="<?php echo $totalPriceWithShippingTax ?>" class="total_price_with_tax">
										</div>
									</div>
									<hr>
									</div><!-- end of cart-detailed-totals -->
									<div class="checkout cart-detailed-actions card-block">
										<div class="text-xs-center">
											<button type="button" id="checkoutClick" <?php if(empty($_SESSION['pincode'])){ echo 'disabled="disabled"' ;}?> class="btn black-btn">Checkout</button>				  
										</div>
									</div>
								</form>
							<div id="block-reassurance">
								<ul>
									<li>
										<div class="block-reassurance-item">
										<!-- <span class="reassurance-img">
											<img src="ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)">
										</span> -->
										<span class="reassurance-img icon-20">
												<img src="img/terms_and_condition.png" alt="TERMS OF SERVICE">
											</span>
										<span><a href="https://static1.squarespace.com/static/557e2414e4b00283cf24caf9/t/5bd114247817f75a062fb3ff/1540428839516/Terms+Of+Service+-+Pixalux+Manufacturing+Australia.pdf">TERMS OF SERVICE</a></span>
										</div>
									</li>
									<li>
										<div class="block-reassurance-item">
											<span class="reassurance-img">
												<img src="img/ic_verified_user_black_36dp_1x.png" alt="PRIVACY POLICY (edit with Customer reassurance module)">
											</span>
											<span><a href="https://www.pixaluxmanufacturing.com.au/s/Privacy-Policy-Pixalux-Manufacturing-Australia.pdf">PRIVACY POLICY</a></span>
										</div>
									</li>
									<li>
										<div class="block-reassurance-item">
										<span class="reassurance-img">
											<img src="img/ic_swap_horiz_black_36dp_1x.png" alt="REFUNDS & RETURNS (edit with Customer reassurance module)">
										</span>
										<span><a href="https://www.pixaluxmanufacturing.com.au/s/Refunds-Return-Policy-Pixalux-Manufacturing-Australia.pdf">REFUNDS & RETURNS</a></span>
										</div>
									</li>
									<li>
										<div class="block-reassurance-item">
											<span class="reassurance-img">
												<img src="img/ic_local_shipping_black_36dp_1x.png" alt="SHIPPING POLICY (edit with Customer reassurance module)">
											</span>
											<span><a href="https://www.pixaluxmanufacturing.com.au/s/Shipping-Policy-Pixalux-Manufacturing-Australia.pdf">SHIPPING POLICY</a></span>
										</div>
									</li>
								</ul>
					  		</div><!-- end of block-reassurance -->
						</div><!-- end of card cart-summary -->	
					</div><!-- end of right-box -->
				</div><!-- end of col -->
			</div><!-- end of row -->
		</div><!-- end of container -->
		<div class="container catalogue" style="margin-top : 100px;">
			<div class="row">
				<div class="col-md-12">
					<h2 id="accessories">Choose your accessories</h2>
				</div>
			<?php 
			$i=1;
		$query = "SELECT * FROM catalogue ";
		$stmt = $db->prepare($query);
		//$stmt->bindValue(':post', $pincode, PDO::PARAM_STR);
		$stmt->execute();
								// get retrieved row
		$frequently = $stmt->fetchall(PDO::FETCH_ASSOC);
		//echo '<pre>';print_r($frequently);echo '</pre>';
		foreach($frequently as $value){
			//if(!in_array($value['SKU'],$_SESSION['catalauge'][$ip])){
			$file_name = explode(',',$value['File-Name']);
			if(count($file_name) > 0){
				$file_name = $file_name[0];
			}
			?>
				<div class="col-md-4">
					<div class="cat_item clearfix">
						<span class="product-img">
							<img src="img/<?php echo $file_name;?>" class="img-responsive"/>
						</span>
						<h5><?php echo $value['Item']?></h5>
						<span class="price-txt">Price : <label>$ <?php echo number_format($fixedCalculation * $value['price'],2);?></label></span>
						<form method="get" class="shopAccessory-<?php echo $value['ID'] ?>" action="cart.php">
							<div class="input-group bootstrap-touchspin">
								<div class="form-group">
									<input  type="hidden" name="additional" value="<?php echo $value['ID'] ?>"/>
									<input id="qty" type="text"  value="1"  name="qty" class="col-md-8 form-control ">
								</div>
							</div>
							<button class="btn cat_btn btn-shopping addAccessory"  data-acessory="<?php echo $value['ID'] ?>" type="submit" >Add to Cart</button>
						</form>
					</div>
				</div>
				<?php
				if (($i % 3) == 0) {
				?>
					</div>
					<div class="row">
				<?php
				}
			//} 
			$i++;
		} 
		?>
			</div>
		</div>
	</section><!-- end of cart -->
	<?php include("footer.php")?>
		<!--<footer style="background-color: #98999A;">
			<div class="container">
				<div class="row copyright">
					<div class="col-md-6">
						<p class="mb-md-0 text-center text-md-left">&copy;2018 PIXALUX MANUFACTURING AUSTRLIA</p>
						<p class="mb-md-0 text-center text-md-left">BROWSER CAPATIBLE WITH <i class="fa fa-chrome"></i> <i class="fa fa-firefox"></i></p>
					</div>
					<div class="col-md-6">
						<p class="credit mb-md-0 text-center text-md-right">Created by
							<a href="https://www.Leapfrogmarket.com.au">Leapfrog Market</a>
						</p>
					</div>
				</div>
			</div>
		</footer> --><div class="modal" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <button type="button" id="successButton" class="btn btn-primary">Save changes</button>
        <button type="button" id="cancleButton" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



  <!-- JavaScript files-->
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery/jquery.min.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/popper.js/umd/popper.min.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/jquery.cookie/jquery.cookie.js">
  </script>
  <script src="http://pixalux.totalsimplicity.com.au/vendor/lightbox2/js/lightbox.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/front.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/js/svg.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/app.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/designs/read-designs.js"></script>
  <script src="http://pixalux.totalsimplicity.com.au/app/products/create-product.js"></script>
  
  
  <link href="css/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" media="all">
   <script src="js/jquery.bootstrap-touchspin.js"></script>
   <script src="app/cart.js"></script>
  	<script>
	var items = '<?php echo count($_SESSION[$ip])?>';
	    $("input[name='qty']").TouchSpin({
	        min: 1,
	        max: 10,
	        stepinterval: 50,
	        maxboostedstep: 100
		});
		
		$('document').ready(function(){
			$('#pincodeForCart').on('focusout',function(){
				var pincode = $(this).val()
				url = home_url + "/drawing/read_postcode.php?postcode=" + pincode;
				$.getJSON(url, function (data) {
					if(!data.status){
						alert('Please enter Postcode for Australia.')
						return false
					}else{
					window.location = "/cart.php?pincode="+pincode;
					}
				})
			});
			
			$('#checkoutClick').on('click',function(){
				var $pannelInCart = '<?php echo $pannelInCart;?>';	 
				var $accessoriesInCart = '<?php echo $accessoriesInCart;?>';	
				
				if(!$pannelInCart){
					// Button that triggered the modal
				  // Extract info from data-* attributes
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  var modal = $('#myModal');//$(this)
				  modal.find('.modal-title').text('You don’t have any panels selected');
				  modal.find("#successButton").text('Add panels')
				  modal.find("#cancleButton").text('I don’t need any panels')
				  modal.modal('show')
				}else if(!$accessoriesInCart){
					// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
					// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
					var modal = $('#myModal');//$(this)
					modal.find('.modal-title').text('You don’t have any accessories selected');
					modal.find("#successButton").text('Add accessories')
					modal.find("#cancleButton").text('I don’t need any accessories')
					modal.modal('show')
				}else{
					if(items > 0)
						$("#checkoutForm").submit();
				}
			});
			
			$('#successButton').on('click',function(){
				var $pannelInCart = '<?php echo $pannelInCart;?>';	 
				var $accessoriesInCart = '<?php echo $accessoriesInCart;?>';
				if(!$pannelInCart){
					window.location.href = "http://pixalux.totalsimplicity.com.au/";
				}else if(!$accessoriesInCart){
					location.href = "#accessories";
					$('#myModal').modal('hide');
				}
				
			});
			$('#cancleButton').on('click',function(){
				if(items > 0)
					$("#checkoutForm").submit();
			});
			
			
			
		})
		
  	</script>
 
</body>

</html>