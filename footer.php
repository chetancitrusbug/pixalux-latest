<footer style="background-color: #98999A;">
	<div class="container">
		<div class="row copyright">
			<div class="col-md-6">
				<p class="mb-md-0 text-center text-md-left">&copy;2018 PIXALUX MANUFACTURING AUSTRALIA</p>
				<p class="mb-md-0 text-center text-md-left" style="font-size: 12px;">Best browsing experience with&nbsp;&nbsp;<a href="https://www.google.com/chrome/"><i class="fa fa-chrome fa-2x"></i></a>&nbsp;&nbsp;or&nbsp;&nbsp;<a href="https://www.mozilla.org/en-US/firefox/new/"><i class="fa fa-firefox fa-2x"></i></p></a>
			</div>
			<div class="col-md-6">
				<p class="credit mb-md-0 text-center text-md-right">Created by
					<a href="https://www.Leapfrogmarket.com.au">Leapfrog Marketing</a>
				</p>
			</div>
		</div>
	</div>
</footer>